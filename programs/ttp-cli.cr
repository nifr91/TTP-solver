#!/usr/bin/env crystal 

# Ricardo Nieto Fuentes 
# nifr91@gmail.com 


require "../src/ttp-object/ttp.cr" 
require "../src/ttp-init/ttp-sol-init.cr"

require "../src/local-search/bitflip-ls.cr" 
require "../src/local-search/bitflip-opt-ls.cr" 

require "../src/iterated-local-search/bitflip-opt-ils.cr"
require "../src/guided-local-search/bitflip-opt-gls.cr"
require "../src/guided-local-search/bitflip-opt-res-gsl"

require "../src/dma/dma.cr"
require "../src/elitist-ma/elitist-ma.cr"
require "option_parser" 

LibC.signal Signal::INT.value,->(s : Int32){exit} 

# Initialization ==============================================================

opt    = Parse.options(ARGV)
errors = Parse.opt_errors(opt)
raise "Parsing error : #{errors}" unless errors.none?

ttp = TTP.load(opt.in_file) 

logger = Logger.new(logsol: true, logevo: !opt.evo_file.empty?) 
logger.span = opt.evo_span 

obj_fun = ->ttp.fitness(Thief)
initial_sol = ttp.init_thief(opt.tour_init,opt.plan_init,opt.lkh_exe) 

# Main Program ================================================================

tic = Time.monotonic 

sol = case opt.method 
when .ls? 
  ls = BitFlipOptLS.new(ttp,opt.duration,obj_fun,logger)
  ls.search(initial_sol)
when .ils? 
  ls = BitFlipOptILS.new(ttp,opt.duration,obj_fun,logger) 
  ls.search(initial_sol)
when .gls?
  ls = BitFlipOptGLS.new(ttp,opt.duration,obj_fun,logger) 
  ls.search(initial_sol) 
when .resgls? 
  ls = BitFlipOptResGLS.new(
    ttp,opt.duration,obj_fun,opt.res_span,opt.lkh_exe,logger)
  ls.search(initial_sol)
when .elitistma? 
  ls = ElitistMemeticGA.new(
    ttp,
    obj_fun,
    opt.duration,
    opt.pop_size,
    opt.lkh_exe,
    logger: logger,
    gens: opt.gens,
    tour_init: opt.tour_init,
    plan_init: opt.plan_init)
  ls.search 
when .dma? 
  ls = DMA.new(
    ttp,
    obj_fun,
    opt.duration,
    opt.pop_size,
    opt.lkh_exe,
    logger: logger,
    min_dist: opt.min_dist,
    gens: opt.gens,
    min_ls_time: opt.min_ls_time,
    tour_init: opt.tour_init,
    plan_init: opt.plan_init)
  ls.search
else raise "unknown method #{opt.method}"
end 

toc = Time.monotonic - tic 

# OUTPUT ---------------------------------------------------------------------

file = (opt.out_file.empty?) ? STDOUT : File.new(opt.out_file,"w+")

file.puts <<-HEADER
# #{Time.local}
# Travelling Salesman Problem Solution
# Tour Initialization : #{opt.tour_init}
# Plan Initialization : #{opt.plan_init}
# Method              : #{opt.method}
# Duration            : #{toc} | #{opt.duration}

# Instance 
#{File.basename(opt.in_file,".ttp")}

HEADER

logger.iosol.rewind
IO.copy(logger.iosol,file)
file.close unless file == STDOUT

unless opt.evo_file.empty?
  logger.ioevo.rewind 
  file = File.new(opt.evo_file,"w+")
  IO.copy(logger.ioevo,file)
  file.close 
end 

# Option Parsing Module 
# =============================================================================
module Parse
  @[Flags]
  enum Error
    Instace_File_Not_Found 
    LKH_Library_Not_Found
    Invalid_Min_Dist
  end 

  enum Method 
    LS 
    ILS
    GLS
    RESGLS
    DMA
    ELITISTMA
  end 
  
  extend self

  struct Options 
    property in_file     = ""
    property out_file    = ""
    property evo_file    = ""
    property lkh_exe     = ""
    property duration    = Time::Span.new(0,0,60)
    property evo_span    = Time::Span.new(0,0,1)
    property res_span    = Time::Span.new(0,0,300)
    property pop_size    = 50
    property tour_init   = TTP::TourInit::Nearest
    property plan_init   = TTP::PlanInit::Greedy
    property method      = Method::RESGLS
    property gens        = 500
    property min_ls_time = 1.0
    property min_dist    = 0.05
  end 

  def options(args)
    quit_program = ARGV.empty?
    opt = Options.new 

    parser = OptionParser.parse(args) do |parser| 
      parser.banner = BANNER

      parser.on("--if=",       "[*]   Instancia a resolver") do |s|
        opt.in_file = s
      end

      parser.on("--of=",       "[STDOUT] Archivo que contendrá la solución") do |s|
        opt.out_file = s
      end 

      parser.on("--duration=", "[60]s Duración de la búsqueda") do |s|
        opt.duration = s.to_f.seconds
      end 
      
      parser.on("--lkh-file=", "[nil]  LKH library executable") do |s|
        opt.lkh_exe = s
      end 
  
      parser.on("--tour-init=", "[Nearest] Tour Initialization heuristic") do |s| 
        opt.tour_init =  TTP::TourInit.parse(s)
      end 
      
      parser.on("--plan-init=", "[Greedy] Plan initialization heuristic") do |s|
        opt.plan_init = TTP::PlanInit.parse(s)
      end 

      parser.on("--evo-file=", "Archivo en el que se guarda la evolución") do |s|
        opt.evo_file = s
      end

      parser.on("--evo-span=", "[1]s Tiempo entre mediciones de la evolución") do |s|
        opt.evo_span = s.to_f.seconds
      end  

      parser.on("--method=","[RESGLS] Método a utilizar para la solución") do |s|
        opt.method = Method.parse(s)
      end 
      
      parser.on("--res-span=", "[300]s Tiempo sin mejora para un reinicio") do |s|
        opt.res_span = s.to_f.seconds 
      end 

      parser.on("--gens=", "[500] número de generaciones") do |s|
        opt.gens = s.to_f.to_i 
      end 

      parser.on("--min-ls-time=","[1.0]s Tiempo mínimo de cada búsqueda local") do |s|
        opt.min_ls_time = s.to_f
      end 

      parser.on("--min-dist=","[0.5] Distancia mínima inicial para penalización" ) do |s|
        opt.min_dist = s.to_f 
      end 

      parser.on("-v","--version", "Muestra la versión") do
        puts  "#{NAME} #{VERSION} #{DATE}"
        exit 
      end

      parser.on("-h","--help", "Mostrar esta ayuda") do
        puts parser
        exit
      end

      parser.invalid_option do |flag|
        puts "Opción invalida: #{flag}"
        puts "Para mas información -h"
        exit 
      end
    end  
    
    (puts(parser) ||  exit) if quit_program
    
    return opt
  end 
  
  def opt_errors(opt)
    error = Error::None
    error |= Error::Instace_File_Not_Found unless File.exists?(opt.in_file)

    if opt.tour_init.lkh? || opt.method.resgls? 
      error |= Error::LKH_Library_Not_Found  unless File.exists?(opt.lkh_exe) 
    end 
    
    unless 0.0 <= opt.min_dist <= 1.0 
      error |= Error::Invalid_Min_Dist
    end 
    
    error  
  end

  VERSION = "1.0"
  NAME    = "TTP SOLVER"
  DATE    = "(18 Apr 2018)"
  AUTHOR  = "Ricardo Nieto Fuentes"
  EMAIL   = "nifr91@gmail.com"

  BANNER = <<-HELP
  Programa encargado de resolver el problema del ladrón viajero o TTP 
  (Travelling Thief Problem) tipo I. 

  Uso :
    Para resolver la instancia 'instancia.ttp' empleando el método por defecto y 
    una duración de 60 segundos 
    
    ./ttp-cli  --if instancia.ttp --duration 60
  
  #{AUTHOR}
  #{EMAIL}

  HELP
end 

