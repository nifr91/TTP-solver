# Ricardo Nieto Fuentes 
# nifr91@gmail.com


module Tournament
  # Torneo binario, selecciona el mejor de dos padres seleccionados al azar 
  # con remplazo de la población. 
  #
  def binary_tournament(pop : Array, fit : Array(Float64))
    a = rand(pop.size)
    b = rand(pop.size) 
    return (fit[a] > fit[b]) ? pop[a] : pop[b]
  end 
end 
