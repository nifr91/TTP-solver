# Ricardo Nieto Fuentes
# nifr91@gmail.com 

require "../ttp-object/ttp.cr"
require "../local-search/local-search.cr"
require "./*"

# La población consiste en un arreglo de ladrones. 
alias Pop = Array(Thief)

# Esta clase representa una plantilla de algoritmo memético de la forma 
#
# ```
# @población = # Ina población
# # Evaluación de `@población`
# until criterio_de_parada
#   hijos = # Cruza 
#   # Búsqueda local `hijos`
#   # Evaluación `hijos`
#   @población = # Selección 
# end 
# return @población 
# ```
#
abstract class Memetic
  extend Tournament 
  extend DiversityMetrics
  
  # Función para inicializar la población 
  abstract def init_pop     (pop_size : Int32) : Pop
  # Función para realizar la cruza entre los elementos de la población 
  abstract def crossover    (pop : Pop, fit : Array(Float64)) : Pop
  # Función para realizar la búsqueda local sobre la población 
  abstract def local_search (pop : Pop) : Pop
  # Función para realizar la selección de las soluciones que pasarán a la 
  # siguiente generación
  abstract def selection    (pop : Pop, childs : Pop) : Pop

  # Población de soluciones 
  getter pop 
  # Fitness de la población de soluciones 
  getter pop_fit   
  # Tamaño de la población
  getter pop_size 

  # Constructor del método, recibe como argumentos, la instancia a resolver, 
  # la función objetivo, la duración, el tamaño de la población y el objeto 
  # para almacenar la evolución de la población. 
  #
  def initialize(
    @ttp      : TTP,
    @obj_fun  : Thief->Float64,
    @duration : Time::Span,
    @pop_size = 50,
    @logger = Logger.new)
    
    @timer   = Timer.new(@duration)
    @gen     = 0u64
    @pop     = [ ] of Thief
    @pop_fit = [ ] of Float64 
  end  

  # Criterio de parada, se considera el tiempo designado para el algoritmo 
  # 
  def stop_criterion 
    @timer.finished? 
  end 
  
  # Ciclo de búsqueda, se encarga de reiniciar los valores de tiempo, 
  # y log, así como de ceder el control del programa hasta que se cumpla el 
  # criterio de parada `#stop_criterion`. En cada iteración se encarga de 
  # almacenar la población
  #
  def search_loop  
    @timer = Timer.new(@duration) 
    @gen   = 0u64
    @logger.clear 
    
    @logger.evolution(@pop,@pop_fit,true)

    until stop_criterion
      yield 
      @gen += 1
      @logger.evolution(@pop,@pop_fit) 
      @logger.it_cnt = @gen
    end 

    @logger.evolution(@pop,@pop_fit,true) 
    best_sol,best_fit = @pop.zip(@pop_fit).max_by{|(sol,fit)| fit }
    @logger.solution(best_sol,best_fit.to_s)
  end 

  # Regresa la población resultante en orden decreciente en fitness. 
  # Realizar por el tiempo establecido la evolución de la población. 
  #  
  def search 
    @pop     = init_pop(@pop_size)
    @pop_fit = eval_pop(@pop)

    search_loop do 
      childs = crossover(@pop,@pop_fit) 
      childs = local_search(childs)
      @pop = selection(@pop,childs)  
      @pop_fit = eval_pop(@pop)
    end 
   
    sorted_pop(@pop,@pop_fit)
  end 

  # Función para evaluar la población, empleando la función objetivo 
  #
  def eval_pop(pop : Pop) : Array(Float64) 
    pop.map{|e| @obj_fun.call e} 
  end 

  # Regresa la población ordenada por fitness 
  #
  def sorted_pop(pop : Pop, pop_fit : Array(Float64))
    (0 ... pop.size).to_a.sort_by!{|i| pop_fit}.reverse!.map{|i| pop[i]}  
  end 
end 
