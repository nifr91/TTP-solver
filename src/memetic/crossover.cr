# Ricardo Nieto Fuentes
# nifr91@gmail.com 

module Crossover
  extend self 

  # Cruza de recombinación de arista, se encarga de preservar la mayor 
  # cantidad de información que contienen los recorridos de los padres. 
  #
  def edge_recombination(a_tour : Tour,b_tour : Tour)
    len = a_tour.size 
    neighbor_list = Array.new(len){ [] of Int32 } 

    a_list = neighbor_list.clone 
    b_list = neighbor_list.clone 

    (0 ... len).each do |i| 
      a_list[a_tour[i]].push(a_tour[(i+1) % len])
      b_list[b_tour[i]].push(b_tour[(i+1) % len])
    end 
    
    (0 ... len).each {|i| neighbor_list[i] = a_list[i] | b_list[i] }

    child_tour = [] of Int32
    cities     = (0 ... len).to_a
    x          = 0

    loop do 

      child_tour.push(x) 
      break if child_tour.size >= len 
      
      cities.delete(x)
      cities.each {|city| neighbor_list[city].delete(x)} 
    
      x = if neighbor_list[x].empty?
        cities.sample 
      else 
        neighbor_list[x].shuffle!.min_by{|city| neighbor_list[city].size} 
      end 
    end 

    child_tour 
  end

  # Operador and 
  #
  def arithmetic_and(a_plan : Plan, b_plan) 
    (0 ... a_plan.size).map{|i| a_plan[i] && b_plan[i] }
  end 
end 
