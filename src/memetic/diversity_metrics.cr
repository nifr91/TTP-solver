# Ricardo Nieto Fuentes
# nifr91@gmail.com

module DiversityMetrics

  # Calcula la distancia promedio entre individuos de la población 
  # 
  def average_distance(pop : Pop) 
    avg_dist = [ 0.0,0.0 ]
    
    pop.size.times do |k| 
      avg_dist_to_pop = [ 0.0,0.0 ]
      
      pop.size.times do |l| 
        next if k == l
        t_dist,p_dist = Thief.distance(pop[k],pop[l]) 
        avg_dist_to_pop[0] += t_dist 
        avg_dist_to_pop[1] += p_dist 
      end 
      
      avg_dist[0] += (avg_dist_to_pop[0] / (pop.size-1) )
      avg_dist[1] += (avg_dist_to_pop[1] / (pop.size-1) ) 
    end 
   
    {avg_dist[0] /pop.size , avg_dist[1] / pop.size } 
  end 


  # Calcula la entropía de la población la cual se define como la suma 
  # de la entropía de cada locus. 
  # 
  def entropy(pop : Pop) 
    s = ->(p : Float64){ (p == 0.0) ? 0.0 : (p * Math.log2(p)) }    
    
    ncities = pop[0].tour.size 
    nitems  = pop[0].plan.size 
    
    tour_loc_prob = Array.new(ncities){Array.new(ncities,0.0)}
    plan_loc_prob = Array.new(nitems,0.0)

    # Obtener las probabilidades 
    pop.each do |sol| 
      tour = sol.tour 
      plan = sol.plan 

      (0...ncities).each {|i| tour_loc_prob[i][tour[i]] += 1.0 }
      (0...nitems).each  {|i| plan_loc_prob[i] += plan[i] ? 1.0 : 0.0 } 
    end 

    # Normalizar 
    tour_loc_prob.map!{|loc| loc.map!{|nc| nc / pop.size} }
    plan_loc_prob.map!{|loc| loc / pop.size }
    
    # Calcular la entropía de cada locus 
    tour_loc_entropy = tour_loc_prob.map do |loc| 
      - loc.reduce(0.0){|acc,val| acc + s.call(val) } 
    end 
    
    plan_loc_entropy = plan_loc_prob.map do |val| 
      - (s.call(val) + s.call(1.0 - val) )
    end 

    tour_s = tour_loc_entropy.reduce(0.0){|acc,h| acc + h} 
    plan_s = plan_loc_entropy.reduce(0.0){|acc,h| acc + h} 
  
    {tour_s,plan_s} 
  end 


end 
