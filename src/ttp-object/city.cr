# Ricardo Nieto Fuentes 
# nifr91@gmail.com

struct City 

  # Primer coordenada 
  getter x
  # Segunda coordenada 
  getter y 
  # Etiqueta que identifica a la ciudad
  getter id

  def initialize(@id : Int32, @x : Float64 , @y : Float64)
  end 
  
  # Distancia euclideana entre dos ciudades 
  # 
  def self.euc_dist(a : City, b : City) 
    (( (a.x - b.x)**2 + (a.y - b.y)**2 )**0.5).ceil 
  end

  # Copia profunda 
  #
  def clone 
    self 
  end  
end 
