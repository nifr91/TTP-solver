# Ricardo Nieto Fuentes 
# nifr91@gmail.com


# Representación de un recorrido
alias Tour = Array(Int32)

# Representación de un plan de recolección 
alias Plan = Array(Bool)

# Clase que representa una solución al problema TTP, consiste en un 
# recorrido a realizar y una selección de objetos a tomar en cada ciudad.
#
class Thief
  
  # Recorrido que realiza el ladrón  
  property tour 
  # Selección de objetos 
  property plan 

  # Genera un recorrido y plan vacíos
  #
  def initialize
    @tour = [ ] of Int32
    @plan = [ ] of Bool
  end 

  # Captura el recorrido y el plan 
  #
  def initialize(@tour : Tour, @plan : Plan) 
  end

  # Crea un objeto a partir de un string con el formato : 
  #
  # Primero las n ciudades del recorrido separadas por `lmnt_separator`
  # , después se separa el recorrido del plan mediante `tp_separator` y
  # finalmente el plan de recolección indicando '1' si se recoge el i-ésimo 
  # objeto o con '0' si se ignora, cada objeto separado por `lmnt_separator`
  #
  # Ejemplo : 
  #
  # ~~~text
  #              plan 
  #          -----------
  # "1,2,3,4 1,1,0" #=> @Thief<xx> @tour: [0,1,2,3] @plan[true,true,false]
  #  -------
  #  Recorrido 
  # ~~~
  #
  #
  def initialize(str : String,tp_separator = " ", lmnt_separator=",")
    tour_str,plan_str = str.split(tp_separator)
    @tour = tour_str.strip.split(lmnt_separator).map(&.to_f.to_i.-(1))
    @plan = plan_str.strip.split(lmnt_separator).map(&.==("1")) 
  end

  # Copia profunda del ladrón 
  #
  def clone
    Thief.new(@tour.clone,@plan.clone)
  end 

  # Dos ladrones son iguales si tienen los mismos objetos y el mismo recorrido
  #
  def ==(other : Thief)
    ( (@tour == other.tour) && (@plan == other.plan) )
  end

  
  # Inversión del segmento del recorrido especificado por el rango `range`
  #
  def reverse!(range : Range(Int32,Int32)) 
    r = range.begin
    l = (range.exclusive?) ? (range.end - 1 ) : range.end 

    raise "reverting the origin yields an invalid solution" if r == 0

    while r < l 
      @tour.swap(r,l)
      r += 1
      l -= 1
    end  
    
    return self
  end 

  # IO -----------------------------------------------------------------------
 
  # Genera una cadena de texto que representa el plan de recolección 
  #
  # ~~~
  # Thief.new(_,[true,true,false,true]) #=> "1,1,0,1" 
  #
  def plan_str
    @plan.map{|e| e ? "1" : "0"}.join(",") 
  end 
 
  # Genera una cadena de texto que representa el recorrido 
  #
  # ~~~ 
  # Thief.new([0,1,2],_).tour_str #=> "1,2,3"
  # ~~~
  # 
  def tour_str 
    @tour.map{|e| e + 1}.join(",")
  end 

  # Imprime en io información sobre el objeto 
  #
  def inspect(io)
    to_s(io) 
  end 

  # Genera una cadena de texto que representa al objeto
  #
  # ~~~
  # Thief.new([0,1,2],[true,true]).to_s #=> "1,2,3 1,1"
  # ~~~
  #
  def to_s(io)
    io << tour_str << " " << plan_str
  end 
 
  # Class Methods -------------------------------------------------------------

  # Extrae de un archivo las solución al una instancia del TTP. 
  #
  # Regresa la solución y el nombre de la instancia que representa. 
  #
  def self.load(file_name : String)
    lines = [""] 

    File.open(file_name, "r") do |file| 
      lines = file.each_line.map(&.strip).
        reject{|l| l.starts_with?("#") || l.empty?}.to_a  
    end  
   
    inst,tour,plan = lines

    {Thief.new("#{tour} #{plan}"),inst} 
  end 

  # Calcula la distancia entre dos solucione en función de la cantidad de 
  # aristas compartidas entre los recorridos. 
  # 
  def self.tour_edge_dist(a_thief : Thief, b_thief : Thief)
    a_tour  = a_thief.tour
    b_tour  = b_thief.tour
    ncities = a_tour.size

    aux = Array.new(ncities,0)

    (0 ... ncities).each {|i| aux[a_tour[i]] = a_tour[(i+1) % ncities]} 

    tour_distance = (0 ... ncities).reduce(0) do |acc,i| 
      acc + ( (aux[b_tour[i]] == b_tour[(i+1) % ncities]) ? 0 : 1 ) 
    end 
  end 


  # Calcula la distancia entre dos soluciones en función del desplazamiento 
  # relativo de cada ciudad en el recorrido. 
  #
  def self.tour_pos_dist(a_thief : Thief, b_thief : Thief) 
   
    a_tour = a_thief.tour 
    b_tour = b_thief.tour
    ncities = a_tour.size 

    # Obtenemos la posición de cada ciudad en uno de los recorridos 
    old_pos = (0 ... ncities).to_a 
    (0 ... ncities).each{|pos| old_pos[a_tour[pos]] = pos } 
    
    # Calculamos la sumatoria de la diferencia relativa de posiciones 
    tour_distance = (0 ... ncities).reduce(0) do |acc,pos|
      city = b_tour[pos] 
      acc + (pos - old_pos[city]).abs
    end 

  end 


  # Calcula la distancia entre dos soluciones en función de la diferencia 
  # de objetos seleccionados en cada plan.  
  # 
  def self.plan_bit_dist(a_thief : Thief, b_thief : Thief) 
    a_plan = a_thief.plan 
    b_plan = b_thief.plan
    nitems = a_plan.size 
    
    plan_distance = (0 ... nitems).reduce(0) do |acc,i|
      acc + ((a_plan[i] == b_plan[i] ) ? 0 : 1)
    end 
  end 


  # Calcula la distancia entre dos soluciones, en función de la cantidad de 
  # aristas compartidas entre los recorridos y la cantidad de objetos en común 
  # en los planes de recolección 
  # 
  # Regresa `{Int32,Int32}` la diferencia del recorrido y plan respectivamente
  #
  def self.distance(a_thief : Thief, b_thief : Thief)
    
    plan_distance = Thief.plan_bit_dist(a_thief,b_thief)  
    tour_distance = Thief.tour_edge_dist(a_thief,b_thief) 
   
    {tour_distance,plan_distance} 
  end 


  # Perturbación para el recorido,
  #
  # ```text
  #             s1                                   s1
  #         a   🡺   b                           a   🡺   b
  #       /            \                       /            \
  #     h                c       4-swap      d                g
  # s4 🡹                 🡻 s2     ⟹       s2 🡹                 🡻 s4
  #     g                d                   c                h
  #       \            /                       \             /
  #         f   🡸   e                            f   🡸   e
  #             s3                                   s3
  #
  #    [a..b, c..d, e..f, g..h]            [a..b, g..h, e..f, c..d]
  #      s1    s2    s3    s4                s1    s4    s3    s2
  #
  # ```
  # 
  def self.tour_kick(thief : Thief, seg_num = 4)
    ncities      = thief.tour.size
    kicked_thief = thief.clone
    kicked_tour  = kicked_thief.tour
    
    # Calcular de forma aleatoria los segmentos 
    seg = (1 ... ncities).to_a.sample(seg_num-1)
    seg.push(0,ncities).sort!

    # Se generan los rangos que abarcan los segmentos a..b,c..d,etc 
    segments = Array.new(seg_num) do |i| seg[i] .. seg[i+1]-1 end 

    # Se genera la permutación k-swap y se genera el nuevo recorrido 
    permutation = segments.dup 
    permutation[1 .. -1] = segments[1 .. -1].reverse! 
    seg_num.times{|l| kicked_tour[permutation[l]] = thief.tour[segments[l]] }
    
    kicked_thief
  end 


  # Iterators -----------------------------------------------------------------
 
  # Regresa un iterador sobre las aristas del recorrido 
  #  
  def edges 
    EdgeIterator.new(self)
  end  
  
  # Iterador sobre las aristas del recorrido 
  #  
  private class EdgeIterator
    include Iterator(Tuple(Int32,Int32)) 
    @tour : Tour 
    
    # Constructor
    #
    def initialize(thief : Thief)
      @tour = thief.tour
      @ptr  = 0
      @ended = false 
    end 
    
    # Reinicio del iterador  
    #
    def rewind 
      @ptr = 0
      @ended = false
    end 
    
    # Se obtiene la siguiente arista `{i-1,i}` 
    #
    def next
      return Iterator.stop if @ended 

      @ptr += 1 

      if @ptr == @tour.size 
        @ptr   = 0
        @ended = true 
      end  

      {@tour[@ptr - 1],@tour[@ptr]}
    end 
  end 
 
  # Regresa un iterador sobre los ids de los objetos seleccionados 
  #
  def items_ids
    ItemIdsIterator.new(self)
  end 

  # Iterador sobre los ids de los objetos sleccionados 
  #
  private class ItemIdsIterator 
    include Iterator(Int32) 
    @plan : Plan 
   
    # Constructor  
    #
    def initialize(thief : Thief)
      @plan = thief.plan 
      @ptr  = -1 
    end 
    
    # Reinicia el iterador
    #
    def rewind 
      @ptr = -1
    end 

    # Obtiene el siguiente elemento seleccionado en el plan de recolección 
    # 
    def next
      @ptr += 1 

      while @ptr < @plan.size && !@plan[@ptr]
        @ptr += 1 
      end 
      
      (@ptr < @plan.size) ? @ptr : Iterator.stop 
    end 
  end 

end 
