# Ricardo Nieto Fuentes 
# nifr91@gmail.com

struct Item 
  
  # Etiqueta del objeto
  getter id 
  # Valor del objeto 
  getter val 
  # Peso del objeto 
  getter wgt 
  # Ciudad en la que se encuentra el objeto
  getter city
  
  def initialize(
    @id   : Int32,
    @val  : Int32,
    @wgt  : Int32,
    @city : Int32)
  end 

  # Copia profunda
  #
  def clone
    self
  end 
end 
