# Ricardo Nieto Fuentes
# nifr91@gmail.com

class TTP 
  
  # Cálculo de la distancia 
  #
  def dist(row,col) 
    City.euc_dist(cities[row],cities[col])
  end 

  # Función objetivo del TTP , debe maximizarse, representa la ganancia final 
  # de vender los objetos recolectados y pagar la renta de transportarlos por 
  # el tiempo que toma recorrer la ruta especificada.
  #
  # ```text
  #                         dz  
  # F(Ξ,Π) =  Σ p + ρ  Σ -------- 
  #          p∈Π      z∈Ξ v(w(z))
  # ```
  #
  def fitness(sol : Thief) 
    
    profit = profit(sol.plan)
    time   = time(sol) 
   
    profit - (@rratio * time)
  end 
    
  # Ganancia que se obtiene por la suma del valor de los objetos recolectados.
  # 
  def profit(plan : Plan)
    (0 ... plan.size).reduce(0.0){|acc,id| acc + (plan[id] ? @items[id].val : 0.0) }  
  end 


  # Peso de los objetos seleccionados en cada ciudad 
  #
  def cities_wgt(plan : Plan) 
    wgt = Array.new(@ncities){0} 

    (0 ... @nitems).each do |id|
      item = @items[id] 
      wgt[item.city] += (plan[id] ? item.wgt : 0 ) 
    end   

    wgt 
  end 


  # Peso acumulado en cada parada del recorrido
  #
  # ~~~text
  #   w[0]=wgt
  #   |        w[1]=w[0]+wgt
  #   |        |              w[-1]=w[-2]+wgt
  #  t[0]  -> t[1]  ->  ... -> t[-1]
  # ~~~
  #
  def acc_wgt(sol : Thief)
    tour = sol.tour 
    plan = sol.plan 

    acc_wgt = Array.new(@ncities){0} 
    wgts = cities_wgt(plan)
    
    acc_wgt[0] = wgts[tour[0]]
    (1 ... @ncities).each {|i| acc_wgt[i] = acc_wgt[i-1] + wgts[tour[i]] }   
    

    acc_wgt   
  end  

  # Tiempo que tarda en visitarse las ciudades del recorrido, el tiempo se ve 
  # afectado por los objetos seleccionados, entre mayor peso se lleve mas lento 
  # se recorre. 
  #
  # ~~~text
  #  tour[0] -> ... -> tour[i] -> ... -> tour[-1]
  #
  #    tour.size - 1
  # tiempo = Σ  tiempo ir de tour[i] a tour[i+1] con peso w[i]                        
  #          0
  #
  # ~~~
  # 
  def time(sol : Thief) 
    tour = sol.tour 
    acc_wgt = acc_wgt(sol) 
  
    t = (0 ... @ncities-1).reduce(0.0) do |acc,i|  
      acc + ( self.dist(tour[i],tour[i+1]) / (@mx_vel - (@nu * acc_wgt[i])) ) 
    end 

    t += ( self.dist(tour[-1],tour[0]) / ( @mx_vel - (@nu * acc_wgt[-1])) )
  end 
  
  
  # Extra ---------------------------------------------------------------------
 
  # Calcula el peso total de la recolección de objetos
  #  
  def wgt(sol : Thief)
    sol.items_ids.reduce(0){|acc,id| acc + @items[id].wgt }
  end 
 
  # Calcula la distancia total del recorrido 
  #  
  def distance(sol : Thief) 
    sol.edges.reduce(0.0){|acc,edge| acc + self.dist(edge[0],edge[1]) }  
  end 

  # Distancia faltante para terminar el recorrido en cada paso.
  #
  # ```text
  #  d[0]= -------------------------------| rem_dist[0]
  #           d[1]= ----------------------| rem_dist[1]
  #                    d[-2]--------------| rem_dist[-2]
  #                           d[-1]= -----| rem_dist[-1]
  #                             
  #  [0]  ->   [x]  ->  ... -> [x]  ->  [0]       
  #   0         1              -1       
  # ```
  #  
  def rem_dist(sol : Thief) 
    tour = sol.tour 
    rem_dist = Array.new(@ncities){0.0}
    
    rem_dist[-1] = self.dist(tour[-1],tour[0])
    (0 ... @ncities-1).reverse_each do |i|
      rem_dist[i] = rem_dist[i+1] + self.dist(tour[i],tour[i+1]) 
    end 

    rem_dist
  end 

  # Distancia faltante para terminar el recorrido en cada ciudad
  # 
  # ```text 
  # d[t[0]] -----------------------------| rem_dist_at_city[tour[0]]
  #                      d[t[1]] --------| rem_dist_at_city[tour[1]]
  #        d[t[-2]] ---------------------| rem_dist_at_city[tour[-2]]
  #                d[t[-1]] -------------| rem_dist_at_city[tour[-1]]
  #
  #  [0]  ->   [x]  ->  ... -> [x]  ->  [0]       
  #   0         1               -1   
  # ```
  #
  def rem_dist_at_city(sol : Thief) 
    tour_rem_dist = rem_dist(sol) 
    city_rem_dist = tour_rem_dist.dup 

    @ncities.times do |index| 
      city_rem_dist[sol.tour[index]] = tour_rem_dist[index] 
    end 

    city_rem_dist
  end 

end 
