# Ricardo Nieto Fuentes 
# nifr91@gmail.com

require "../memetic/memetic.cr" 
require "../utils/multi-dynamic.cr"


class DMA < Memetic 
  include MultD(Thief)

  # Genera la función linealmente creciente del tiempo dedicado a la búsqueda 
  # local en función de la duración de la ejecución. 
  #
  # Consiste de una progresión aritmética: 
  #
  #             (2 (tf - |P| g tm ) 
  # tls = tm +  ------------------- (i-1) 
  #                 g (g -1) |P| 
  #
  # En esta progresión g indica el número de iteraciones a realizar, tm es el 
  # tiempo mínimo dedicado a la búsqueda, tf es el tiempo total del algoritmo e 
  # i es el nímero de iteración actual . 
  #
  # Note que en la primer i = 1 el tiempo es tls = tm, mientras que en la última 
  # i = g el tiempo es el doble del promedio (2 tf) / (g |p|) - tm. 
  #
  # El tiempo total es : 
  #
  #  g        g ( tls_1 + tls_g)     tf
  # Σ tls_i = ------------------- =  ---
  #  1                 2             |P|
  #
  getter ls_span : Proc(Time::Span)

  # Constructor de la clase
  def initialize(
    ttp, 
    obj_fun, 
    duration, 
    pop_size,
    @lkh_exe : String,
    logger=Logger.new, 
    @min_dist = 0.05, 
    gens = 500, 
    min_ls_time = 1.0,
    @tour_init = TTP::TourInit::Nearest,
    @plan_init = TTP::PlanInit::Packiterative)

    super(ttp,obj_fun,duration,pop_size,logger)
    min_ls_time = {min_ls_time,@duration.to_f / @pop_size}.min
  
    @ls_span = ->(){ 
      num = (2*(duration.to_f - (pop_size* gens * min_ls_time)))
      den = (gens * (gens - 1) * pop_size)
      (min_ls_time + ((num / den) * (@gen))).seconds
    } 

    @local_search = BitFlipOptGLS.new(@ttp,@ls_span.call,@obj_fun)
  end 

  # Función para inicializar la población, se emplean las inicializaciones 
  # LKH y Greedy para el recorrido y el plan de recolección respectivamente 
  #
  def init_pop(pop_size = @pop_sze) : Pop
    Array.new(pop_size) do 
      @ttp.init_thief(@tour_init,@plan_init,@lkh_exe)
    end
  end 
  
  # Función de cruza entre dos padres seleccionados por torneo binario 
  # empleando el crossover "Edge Recombination Operator" o ERO en el 
  def crossover(pop : Pop,fit : Array(Float64))  : Pop
    Array.new(pop.size) do |i| 
      a_parent = Memetic.binary_tournament(pop,fit) 
      b_parent = Memetic.binary_tournament(pop,fit)
     
      tour = Crossover.edge_recombination(a_parent.tour,b_parent.tour)
      plan = Crossover.arithmetic_and(a_parent.plan,b_parent.plan)

      Thief.new(tour,plan)
    end 
  end 

  def selection(pop : Pop, childs : Pop) : Pop
    selection(pop,childs,@min_dist * (1.0 - @timer.progress))
  end 
  
  # Evaluación de la población empleando la función `@obj_fun`
  #
  def eval_pop(pop : Array(Thief)) : Array(Float64)
    pop.map{|e| @obj_fun.call(e) }
  end 

  # Aplicar la búsqueda local a cada elemento de la población
  #
  private def local_search(pop : Pop)  : Pop
    @local_search.duration = ls_span.call
    pop.map{|sol| @local_search.search(sol) } 
  end 
  
  # Calcular la distancia entre dos soluciones
  #
  def sol_distance(a : Thief, b : Thief) : Float64
    Thief.distance(a,b)[0].to_f / a.tour.size.to_f
  end 

end 
