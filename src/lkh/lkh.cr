# Ricardo Nieto Fuentes
# nifr91@gmail.com 

require "uuid"

# Módulo para obtener un recorrido mediante la librería [LKH]
# (http://www.akira.ruc.dk/~keld/research/LKH/)
#
module LKH 
  extend self 
  
  class Params 
    property tsp_filename       = ""
    property tour_filename      = ""
    property move_type          = 2
    property time_limit         = 0.5
    property runs               = 1
    property seed               = 0
    property trace_level        = 0
    property max_swaps          = 1
    property max_trials         = 1
    property max_candidates     = 1
    property candidate_set_type = "N"

    def self.new
      params = Params.new
      yield params 
      params
    end 
  end 

  # Crea el archivo que contiene la instancia ttp 
  #
  def tsp_file(ttp : TTP)
    File.tempfile("lkh-tsp-#{UUID.random}") do |file| 
      file.puts "NAME : #{ttp.name}"
      file.puts "COMMENT : No comment"
      file.puts "TYPE : TSP"
      file.puts "DIMENSION : #{ttp.ncities}"
      file.puts "EDGE_WEIGHT_TYPE : CEIL_2D" 
      file.puts "NODE_COORD_SECTION"
      
      ttp.cities.each do |city|
        file.puts "#{city.id + 1} #{city.x.to_i} #{city.y.to_i}"
      end  
    end  
  end 

  # Crea un archivo para almacenar el recorrido resultante
  #
  def tour_file 
    File.tempfile("lkh-tour-#{UUID.random}") 
  end 
 
  # Crea el archivo de parámetros para la libreria LKH
  #
  def params_file(params : Params)
    return File.tempfile("TTP_LKH_params_#{UUID.random}") do  |file|
      file.puts "PROBLEM_FILE = #{params.tsp_filename}"
      file.puts "OUTPUT_TOUR_FILE = #{params.tour_filename}"
      file.puts "MOVE_TYPE = #{params.move_type}"
      file.puts "TIME_LIMIT = #{params.time_limit}"
      file.puts "RUNS = #{params.runs}"
      file.puts "TRACE_LEVEL = #{params.trace_level}"
      file.puts "SEED = #{params.seed}"
      file.puts "MAX_SWAPS = #{params.max_swaps}"
      file.puts "MAX_TRIALS = #{params.max_trials}"
      file.puts "MAX_CANDIDATES = #{params.max_candidates}"
      file.puts "CANDIDATE_SET_TYPE = #{params.candidate_set_type}"
    end
  end

  # Resuelve la ruta considerando solo el problema TSP de la instancia de TTP,
  # empleando la librería LKH
  #
  def tour(ttp_inst : TTP,lkh : String = "lkh.exe", params : Nil | Params = nil)
    tsp_file = LKH.tsp_file(ttp_inst)
    tour_file = LKH.tour_file 

    if params.nil? 
      params = LKH::Params.new do |params| 
        params.tsp_filename       = tsp_file.path
        params.tour_filename      = tour_file.path
        params.move_type          = 5
        params.time_limit         = 5.0
        params.runs               = 1
        params.seed               = rand(Int32::MAX)
        params.trace_level        = 0
        params.max_swaps          = ttp_inst.ncities 
        params.max_trials         = ttp_inst.ncities
        params.max_candidates     = 5
        params.candidate_set_type = "N"
      end
    end 
    
    params_file = LKH.params_file(params)
    log = `./#{lkh}  #{params_file.path}`    
   
    tour = load_tour(tour_file.path) 
     
    tsp_file.delete
    tour_file.delete 
    params_file.delete 

    tour
  end 
  
  # Carga la solución generada por la librería LKH
  #
  def load_tour(tour_filename : String) 
    tour = [ ] of Int32
    File.open(tour_filename) do |file|
      6.times{ _ = file.gets }
      while (line = file.gets) && line != "-1"
        tour.push (line.to_i - 1) 
      end 
    end 
    tour 
  end   
end 

