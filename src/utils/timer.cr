# Ricardo Nieto Fuentes
# nifr91@gmail.com

# Esta estructura representa el paso del tiempo 
#
struct Timer 

  # Tiempo de creación
  getter start_time : Time::Span 

  # Tiempo de termino 
  getter time_limit : Time::Span 

  # Duración del temporizador
  getter duration 

  # El constructor recibe el tiempo de duración
  # 
  def initialize(@duration : Time::Span)
    @start_time = Time.monotonic 
    @time_limit = @start_time + @duration
  end 
  
  # Tiempo transcurrido desde la creación del objeto 
  #
  def elapsed_time
    Time.monotonic - @start_time 
  end

  # Porcentaje de avance entre el tiempo de inicio y el de termino
  #
  def progress 
    1.0 - (time_left.to_f / duration.to_f)
  end
  
  # Indica si se ha cumplido con el tiempo establecido
  #
  def finished?
    (Time.monotonic >= @time_limit)
  end  
  
  # Tiempo restante para cumplir la duración establecida
  #
  def time_left 
    ((time = @duration - elapsed_time) >= 0.seconds) ? time : 0.seconds
  end 

end 
