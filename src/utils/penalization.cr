# Ricardo Nieto Fuentes
# nifr91@gmail.com

# Clase que representa la penalización sobre las características presentes 
# en una solución `s`
# 
abstract class Penalization
  # Regresa el valor de la penalización 
  abstract def call (s)           
  # Actualiza la penalización
  abstract def update(s)
  # Reinicia las penalizaciones 
  abstract def reset
end 

# Penalización sobre las aristas del recorrido. Se penaliza la arista 
# con menor penalización que aporte menos a la solución. 
#
class TourPenalization < Penalization 
  
  # Estructura de característica, representada por la arista `{de,a}` en el 
  # recorrido.
  # 
  private struct Feature
    property from,to,utility 
    def initialize(@from : Int32, @to : Int32, @utility : Float64) 
    end
  end 
  
  # Representación interna de la penalización de cada arista posible
  @ary : Array(Array(UInt64))
  
  # Constructor, las penalizaciones se inician en 0 
  #
  def initialize(@ttp : TTP)
    @ary = Array.new(@ttp.ncities){ Array.new(@ttp.ncities){0u64} }  
  end 
  
  # Regresa la suma de las penalizaciones sobre las características de la 
  # solución `sol`
  #
  def call(sol : Thief)
    sol.edges.reduce(0_u64){|acc,(from,to)| acc + @ary[from][to] }
  end
  
  # Actualiza las penalizaciones en función de las características presentes 
  # en la solución `sol`
  #
  def update(sol : Thief) 
    feat  = Feature.new(0,0,-1.0/0.0) 

    sol.edges.each do |from,to| 
      utility = @ttp.dist(from,to).to_f / ( 1.0 + @ary[from][to].to_f) 
      feat = Feature.new(from,to,utility) if (utility > feat.utility)
    end 

    @ary[feat.from][feat.to] += 1u64
    self 
  end  
  
  # Reinicia las penalizaciones de cada arista
  #
  def reset 
    @ttp.ncities.times{|r| @ttp.ncities.times {|c| @ary[r][c] = 0u64 } } 
    self
  end 
end 
