# Ricardo Nieto Fuentes
# nifr91@gmail.com

# Este módulo implementa la selección mediante el algoritmo multi-dynamic del 
# artículo : 
# 
# "A novel diversity based replacement strategy for evolutionary algorithms" 
# -- Carlos Segura
#
# 
module MultD(T)

  # Distancia entre soluciones 
  abstract def sol_distance(a : T,b : T) : Float64
  # Evaluación de la población  
  abstract def eval_pop(pop : Array(T)) : Array(Float64)

  # Selección de la siguiente generación que promueve la diversidad en función 
  # de la distancia al vecino más cercano (DCN), penalizando aquellas 
  # soluciones que estén muy cerca de otra solución ya seleccionada. 
  # 
  def selection(pop : Array(T),childs : Array(T), min_dist : Float64)
    
    current_members = pop + childs 
    
    # Obtener el mejor individuo en fitness 
    fit = eval_pop(current_members) 
    best_index = (0 ... fit.size).to_a.sort_by!{|i| fit[i]}[-1]
    
    # Se agrega el mejor individuo a la nueva población y se elimina 
    new_pop = [current_members.swap(best_index,-1).pop] 
    fit.swap(best_index,-1).pop 
    
    # Hasta obtener la cantidad necesaria de soluciones, se calcula la 
    # distancia al vecino mas cercano de cada elemento, se penaliza el fintess 
    # para favorecer la diversidad y se selecciona el mejor elemento no dominado
    #
    while new_pop.size < pop.size 
      dcn = current_members.map{|e| distance_to_closest_neighbor(e,new_pop)}
      
      pen_fit = penalize_fitness(fit,dcn,min_dist) 
      
      non_dom_indexes = non_dominated_indexes(pen_fit.zip(dcn)) 

      best_non_dom_index = non_dom_indexes.sort_by!{|e| fit[e] }[-1] 

      new_pop.push(current_members.swap(best_non_dom_index,-1).pop) 
      fit.swap(best_non_dom_index,-1).pop 
    end 
    
    new_pop 
  end 

  
  # Dominancia débil de pareto, se dice que el vector 'a' domina al vector 'b'
  # denotado como `a <= b` si 'a' es mejor o igual que 'b' en todas sus 
  # entradas y es estrictamente mejor en al menos una. 
  #
  def a_dom_b(a : Tuple, b : Tuple)
    eq_cnt = 0

    (0 ... a.size).each do |k| 
      return false if a[k] < b[k] 
      eq_cnt += 1 if a[k] == b[k] 
    end 
    
    (eq_cnt < a.size) ? true : false 
  end 

  # Obtiene los índices de las soluciones no dominadas
  #  
  def non_dominated_indexes(sol_objectives : Array(Tuple) )
    # pop : Array(T), fit : Array(Float64), dcn : Array(Float64))
    
    # sol_objectives = fit.zip(dcn) 
    non_dom = [0] 

    (1 ... sol_objectives.size).each do |candidate_index|
      candidate_is_non_dom = true 
      candidate = sol_objectives[candidate_index]
      
      (0 ... non_dom.size).reverse_each do |k| 
        nd = sol_objectives[non_dom[k]] 
        candidate_is_non_dom = false if a_dom_b(nd, candidate)
        non_dom.swap(k,-1).pop       if a_dom_b(candidate, nd)
      end 

      non_dom.push(candidate_index) if candidate_is_non_dom 
    end 
    
    non_dom 
  end 

  # Penalización de las soluciones en función de la distancia al vecino mas 
  # mas cercano 
  #
  # ```text
  # FIT
  #  |         '  
  #  |    x    '
  #  |    |    ' x
  #  |  x |    '
  #  |  | |  x '
  #  |  v v  v '   x
  #  |_________'________ DCN
  #            '
  #         min_dist 
  # ```
  #
  # Se asigna un mal fitness a las soluciones que tengan un valor de dcn 
  # inferior a `min_dist`, es decir aquellas soluciones que aportan poco a la 
  # diversidad pues estan muy cerca de otra solución.
  # 
  #
  def penalize_fitness(
    fit : Array(Float64), dcn : Array(Float64), min_dist : Float64)

    fit.map_with_index{|f,i|  (dcn[i] < min_dist) ? -1.0/0.0 : f } 
  end 

  # Menor distancia de la solución `sol` al vecino más cercano de la población 
  # `pop`
  #
  def distance_to_closest_neighbor(sol : T, pop : Array(T)) 
    pop.reduce(1.0/0.0){|acc,e| {sol_distance(sol,e),acc}.min } 
  end 

end 
