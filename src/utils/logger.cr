# Ricardo Nieto Fuentes 
# nifr91@gmail.com

# Ricardo Nieto Fuentes 
# nifr91@gmail.com

# Esta clase se encarga de realizar el registro del estado interno y solución 
# final del programa.
#
class Logger
 
  # IO que recibe los datos a log  
  getter iolog = File.tempfile("Logger-log-#{UUID.random}","w+") 
  # Bandera para realizar o ignorar las escrituras a iolog 
  getter loglog = false  
  
  # IO que recibe la evolución de la solución 
  getter ioevo = File.tempfile("Logger-evo-#{UUID.random}","w+")  
  # Bandera para realizar o ignorar las escrituras a ioevo
  getter logevo = false 

  # Archivo que tiene la mejor solución encontrada 
  getter iosol = File.tempfile("Logger-sol-#{UUID.random}","w+") 
  # Bandera para realizar o ignorar las escrituras a iosol
  getter logsol = false

  # Tiempo entre mediciones de la evolución 
  property span : Time::Span = 1.seconds

  # Número de iteraciones 
  property it_cnt = 0u64

  # Número de reinicios
  property res_cnt = 0u64


  # Constructor 
  #
  def initialize(@logevo = false, @loglog = false, @logsol = false)
    @time    = Time.monotonic
    @start   = @time

    (@ioevo.close || @ioevo.delete) unless @logevo 
    (@iolog.close || @iolog.delete) unless @loglog 
    (@iosol.close || @iosol.delete) unless @logsol 

    at_exit do 
      @iolog.delete if File.exists?(@iolog.path)
      @ioevo.delete if File.exists?(@ioevo.path)
      @iosol.delete if File.exists?(@iosol.path)
    end 
  end 

  
  # Indica si se debe realizar la medición (e.g. ha pasado 'span' tiempo desde 
  # la última medición 
  #
  def measure? 
    ((Time.monotonic - @time) >= @span)
  end   
  
  # Log que almacena cualquier dato extra, usualmente usado para 
  # debug.
  #  
  def log(str : String)
    if @loglog  
      @iolog << "# " << elapsed_time << "\n"
      @iolog.puts str 
      @iolog.flush 
    end 
  end 
 
  # Tiempo transcurrido desde la creación de Logger
  # 
  def elapsed_time
    (Time.monotonic - @start).to_f 
  end 

  # Log del estado de la solución y su fitness, así como  el estado 
  # interno de repeticiones e iteraciones realizadas
  #
  # Agrega una línea al archivo @evo :
  #
  # ~~~text
  # # Tiempo Fitness Tour Plan Iteraciones Reinicio
  # 0.0 1.0 1,2,3 0,1,0,1 10 0
  # ~~~
  # 
  def evolution(sol : Thief, fit : Float64,measure = measure?)
    if @logevo 
      if measure
        @ioevo << elapsed_time.round(2) << " "
        @ioevo << fit.round(5)          << " "
        @ioevo << sol.tour_str          << " "
        @ioevo << sol.plan_str          << " "
        @ioevo << @it_cnt               << " "
        @ioevo << @res_cnt              << "\n"
        @ioevo.flush 
        @time = Time.monotonic 
      end 
    end 
  end  

  # Log del estado de la población y su fitness, así como el estado interno 
  # del número de iteraciones realizadas. 
  #
  # Agrega un bloque al archivo @evo : 
  # 
  # ~~~text 
  # 0.0 1.0 0         #=> Tiempo Mejor Fitness #iteraciones
  # 1.0 1,2,3 1,1,1   #=> Fitness Tour Plan del primer individuo 
  # 0.0 1,2,3 0,0,0   #=> ""       ""  ""   del segundo individuo
  # ~~~ 
  #  
  def evolution(pop : Array(Thief), fit : Array(Float64),measure = measure?)
    
    if @logevo  
      if measure 
        @ioevo << elapsed_time << " "
        @ioevo << fit.max << " " 
        @ioevo << @it_cnt  << "\n"
        
        (0 ... pop.size).each do |i| 
          @ioevo << fit[i] << " "
          @ioevo << pop[i] << "\n"
        end 
        @ioevo << "\n" 

        @ioevo.flush

        @time = Time.monotonic 
      end
    end  
  end 

  # Escribe el nombre de instancia y la solución en el archivo `@sol`
  #
  # Agrega un bloque al archivo @iosol : 
  #
  # ~~~text
  # # Instance 
  # una-instancia.ttp
  # # Tour 
  # 1,2,3
  # # Plan 
  # 0,1,0,0,0,0,1
  # # Fitness 
  # 10.0
  # ~~~
  #
  def solution(sol : Thief , fit) 
    if @logsol 
      @iosol.puts "# Tour"
      @iosol.puts sol.tour_str 
      @iosol.puts "# Plan" 
      @iosol.puts sol.plan_str 
      @iosol.puts "# Fitness"
      @iosol.puts fit.to_s
      @iosol.flush
    end 
  end 

  # Cierra todos los archivos 
  #
  def close 
    @iosol.close 
    @ioevo.close 
    @iolog.close 
  end 

  # Limpia los contenidos de los archivos temporales y la cuenta de las 
  # iteraciones
  # 
  def clear
    @it_cnt  = 0u64
    @res_cnt = 0u64
    @ioevo.rewind.truncate if @logevo 
    @iolog.rewind.truncate if @loglog 
    @iosol.rewind.truncate if @logsol 
  end 

end  
