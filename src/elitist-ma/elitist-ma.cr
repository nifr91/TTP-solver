require "../memetic/memetic.cr" 
require "../local-search/bitflip-opt-ls.cr"

class ElitistMemeticGA < Memetic 
  
  @ls_duration : Time::Span
  
  def initialize(
    ttp,
    obj_fun,
    duration,
    pop_size,
    @lkh_exe : String,
    logger = Logger.new, 
    gens = 500,
    @tour_init = TTP::TourInit::Nearest,
    @plan_init = TTP::PlanInit::Packiterative)
    
    super(ttp,obj_fun,duration,pop_size,logger) 
    @ls_duration = (@duration.to_f / (gens * @pop_size)).seconds
    @local_search = BitFlipOptGLS.new(@ttp,@ls_duration,@obj_fun)
  end 

  def init_pop(pop_size = @pop_size) : Pop
    Array.new(pop_size){ @ttp.init_thief(@tour_init,@plan_init,@lkh_exe) }
  end 

  def crossover(pop : Pop, fit : Array(Float64)) : Pop
    Array.new(pop.size) do |i| 
      a_parent = Memetic.binary_tournament(pop,fit) 
      b_parent = Memetic.binary_tournament(pop,fit) 
      tour = Crossover.edge_recombination(a_parent.tour,b_parent.tour) 
      plan = Crossover.arithmetic_and(a_parent.plan,b_parent.plan) 
      Thief.new(tour,plan) 
    end 
  end 

  def local_search(pop : Pop) : Pop 
    @pop.map{|sol| @local_search.search(sol)}
  end 

  def selection(pop : Pop, childs : Pop)  : Pop
    merged = pop + childs
    merged_fit = eval_pop(merged)
    sorted = sorted_pop(merged,merged_fit) 
    sorted[0 ... pop.size]
  end 
end 
