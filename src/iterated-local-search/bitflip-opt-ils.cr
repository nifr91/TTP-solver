# Ricardo Nieto Fuentes 
# nifr91@gmail.com 

require "../local-search/local-search.cr" 
require "../local-search/bitflip-opt-ls.cr" 


# Clase que representa la búsqueda local iterada empleando el framework 
# CoSolver. 
#
# La búsqueda local iterada consiste en una vez que se encuentra un óptimo 
# local mediante la búsqueda local, se perturba la mejor solución. La
# perturbación debe tener un balance de manera que no destruya la solución pero 
# la mueva lo suficiente como para escapar de la zona de atracción. 
# 
# 
class BitFlipOptILS < LS 
  
  def initialize(ttp,duration,obj_fun,logger = Logger.new) 
    super(ttp,duration,obj_fun,logger) 
    @bfoptls = BitFlipOptLS.new(ttp,duration,obj_fun) 
  end 

  def search(sol : Thief)  : Thief
    @best = sol.clone 
    @best_fit = @obj_fun.call(@best) 

    search_loop do 
      @bfoptls.duration = @timer.time_left 
      sol = @bfoptls.search(sol) 
      fit = @obj_fun.call(sol)  
      update_best(sol,fit) if fit > @best_fit 
      sol = Thief.tour_kick(@best) 
    end 

    @best 
  end 
end 
