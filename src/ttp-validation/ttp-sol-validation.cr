# Ricardo Nieto Fuentes
# nifr91@gmail.com

class TTP 

  @[Flags]
  enum ThiefError
    # Errores del recorrido 
    Repeated_cities 
    Cities_mismatch 
    Invalid_cities
    Invalid_origin 
    # Errores del plan de recolección 
    Items_mismatch  
    Exceeded_weight 
    Invalid_items
    # Errores de fitness 
    Fitness_mismatch    
  end

 
  # Validación de una solución 
  # 
  # str es una cadena de texto que representa a la solución con la forma 
  #
  # ~~~
  # "1,2,3,4 0,0,0,1,0,1"
  # ~~~ 
  # 
  # en donde `tp_separator` separa el recorrido del plan de recolección y 
  # `lmnt_separator` separa cada elemento del plan y del recorrido.
  #
  # Un plan válido debe cumplir las siguientes caracteristicas
  #
  # * La cadena de texto debe tener el recorrido seguido por el plan de 
  #   recolección
  # * Las ciudades deben visitarse una sola vez (sin repetición).
  # * El recorrido debe tener la cantidad de ciudades correcta para la 
  #   instancia
  # * El recorrido debe iniciar en la ciudad '1' 
  # * Las ciudades deben ser representadas en el rango `(1 .. #ciudades)`
  # * El peso total de los objetos seleccionados debe ser menor o igual que la 
  #   capacidad 
  # * El plan de recolección debe tener la cantidad de objetos correcta para la
  #   instancia 
  # * Los objetos a recolectar deben ser representados con '1' (seleccionado) o 
  #   '0' (ignorado).  
  #
  def thief_errors(
    str : String, 
    tp_separator     = " ",
    lmnt_separator   = ",")
    
    error = ThiefError::None 

    tour_str,plan_str = str.split(tp_separator,remove_empty: true)
    
    error |= plan_errors(plan_str,lmnt_separator) 
    error |= tour_errors(tour_str,lmnt_separator)

    error 
  end 

  
  # Plan validation
  #
  # * Items must be represented with 0 or 1 
  # * The plan must have the same number of items as the instance
  # * The total weight of selected items must be <= to the capacity 
  #
  # Returns a flag with the errors found in the plan 
  # 
  def plan_errors(str : String,separator = ",") 
    error = ThiefError::None

    vals = str.split(separator,remove_empty: true)

    # Check if it contains the expected number of items
    error |= ThiefError::Items_mismatch unless vals.size == @nitems 

    # Check if is a valid representation 
    unless vals.all?{|e| ((e == "1") || (e == "0"))}
      error |= ThiefError::Invalid_items 
    end 
    
    # Check if the total weight is less or equal than the knapsack capacity
    plan = vals.map{|e| (e == "1")} 
    wgt  = self.wgt(Thief.new [ ] of Int32, plan  ) 
    error |= ThiefError::Exceeded_weight unless wgt <= @mx_wgt 

    error
  end 

  # Tour validation 
  #
  # * Each city must be visited once and only once 
  # * The tour must have the same number of items as the instance 
  # * The tour must start from the city '1'
  # * Cities must be represented with ids form `(1 .. ncities )`
  #
  # Returns a flag with the errors found in the plan 
  # 
  def tour_errors(str : String, separator = ",")
    error = ThiefError::None 

    vals = str.split(separator,remove_empty: true)

    # Check if it contains the expected number of cities 
    error |= ThiefError::Cities_mismatch unless vals.size == @ncities
    
    # Check if is a valid representation 
    valid_cities = (1 .. @ncities).map(&.to_s).to_a 
    unless vals.all?{|e| valid_cities.includes?(e)} 
      error |= ThiefError::Invalid_cities
    end  
   
    # Check if there are repeated cities 
    error |= ThiefError::Repeated_cities unless vals.uniq.size == vals.size
    
    # Check if the tour start from the origin 
    error |= ThiefError::Invalid_origin unless vals[0] == "1"
    
    error
  end 


  # Validación del resultado de fitness se compara el valor `fit` 
  # frente al cálculo. Se considera correcto si son iguales empleando una 
  # tolerancia de  `0.001`. 
  # 
  def fitness_errors(
    fit : String, 
    thief : String, 
    tp_separator = " ",
    lmnt_separator = ",") 
     
    expected_fitness = self.fitness(Thief.new(thief,tp_separator,lmnt_separator))
    
    if ((expected_fitness - fit.to_f).abs <= 1e-3)
      ThiefError::None 
    else 
      ThiefError::Fitness_mismatch
    end 
  end 

end 
