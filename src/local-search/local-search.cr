# Ricardo Nieto Fuentes
# nifr91@gmail.com

require "../utils/logger.cr"
require "../utils/timer.cr"

# Esta clase representa una plantilla de búsqueda local de la forma 
#
# ~~~
# @mejor = una solución 
# until criterio_de_parada 
#   # heurística de búsqueda 
#   # actualizando @mejor = la mejor solución encontrada 
# end 
# return @mejor
# ~~~
#
#
abstract class LS

  # Duración de la búsqueda local 
  property duration = 5.seconds 
  # La mejor solución encontrada
  @best = Thief.new 
  # El fitness de la mejor solución encontrada 
  @best_fit = Float64::MIN
  
  # Bandera que indica si existe mejora  
  @improves = true 
  # Temporizador de la búsqueda local 
  @timer   = Timer.new(0.seconds)

  # Constructor , recibe la instancia a resolver, la duración de la búsqueda 
  # la función objetivo y opcionalmente un objeto de registro para almacenar
  # información o la evolución de la búsqueda local
  #
  def initialize(
    @ttp      : TTP, 
    @duration : Time::Span,
    @obj_fun  : Thief->Float64, 
    @logger   = Logger.new)
  end 

  # Criterio de parada, se considera como criterio de parada que el tiempo 
  # designado para la búsqueda local terminara o que la bandera de mejora 
  # sea `false` (e.g. `@improves = true if` se recorre la vecindad sin 
  # encontrar mejora)
  #
  def stop_criterion 
    @timer.finished? || !@improves 
  end 

  # Ciclo de búsqueda reinicia los valores de tiempo, número de iteración y 
  # la bandera `improves` a los valores por defecto y `yields` hasta que se 
  # cumpla el criterio de parada. `#stop_criterion`. En cada iteración se 
  # encarga de almacenar la evolución en el objeto de registro.
  #
  def search_loop
    @timer          = Timer.new(@duration)
    @logger.it_cnt  = 0u64
    @logger.res_cnt = 0u64
    @improves       = true

    until stop_criterion 
      yield
      @logger.evolution(@best,@best_fit)
      @logger.it_cnt += 1 
    end 

    @logger.evolution(@best,@best_fit)
  end

  # Esta función se encarga de actualizar la mejor solución encontrada 
  # almacenando una copia profunda en la variable `@best`
  #
  def update_best(best : Thief, fit : Float64) 
    @best = best.clone 
    @best_fit = fit 
  end   
  
  # Función que debe ser implementada por todas las clases derivadas
  # Se encarga de realizar la búsqueda sobre la definición de vecindad, manejar
  # la bandera del criterio de paro y actualizar la mejor solución encontrada 
  # en cada mejora. 
  #
  abstract def search(sol : Thief) : Thief
end 
