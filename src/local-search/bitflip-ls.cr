# Ricardo Nieto Fuentes 
# nifr91@gmail.com 

require "./local-search"


# Clase que representa la búsqueda local por escalada estocástica de la 
# vecindad Bit-Flip.
#
class BitFlipLS < LS 

  # Función de búsqueda mientras no se cumpla el criterio de paro se 
  # considera cada vecino bit-flip de la mejor solución encontrada.
  # 
  def search(sol : Thief)  : Thief
    @best = sol.clone 
    @best_fit = @obj_fun.call(@best)  

    search_loop do
      @improves = false

      each_neighbor_of(@best) do |neighbor| 
        fit = @obj_fun.call(neighbor)

        if fit > @best_fit
          update_best(neighbor,fit)
          @improves = true 
          break
        end 
      end 
    end 

    @best
  end 

  # Cede al bloque de forma aleatoria cada elemento de la vecindad `N(s)`. 
  # La vecindad Bit-Flip consiste cambiar el estado de cada objeto 
  # (e.g. si se lleva se deja, si no se llevaba pasa a llevarse). 
  # La vecindad considera solo los objetos que cumplen la restricción de peso
  #  
  def each_neighbor_of(sol : Thief) 
    neighbor = sol.clone 
    plan = neighbor.plan 
    ids = (0 ... @ttp.nitems).to_a.shuffle! 
    wgt = @ttp.wgt(neighbor) 

    ids.each do |id|
      if plan[id] || ((wgt + @ttp.items[id].wgt) <= @ttp.mx_wgt)
        plan[id] = !plan[id] 
        yield neighbor,id 
        plan[id] = !plan[id] 
      end 
    end 
  end
end 

