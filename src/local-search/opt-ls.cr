# Ricardo Nieto Fuentes 
# nifr91@gmail.com

require "./local-search"

# Clase que representa la búsqueda local por escalada estocástica de la 
# vecindad k-opt.
#
class OptLS < LS 

  # Función de búsqueda, mientras no se cumpla el criterio de paro, 
  # se considera cada vecino k-opt de la mejor solución encontrada.
  #
  def search(sol : Thief) : Thief
    @best = sol.clone 
    @best_fit = @obj_fun.call(@best)

    search_loop do 
      @improves = false

      each_neighbor_of(@best) do |neighbor| 
        fit = @obj_fun.call(neighbor)

        if fit > @best_fit 
          update_best(neighbor,fit)
          @improves = true
          break
        end 
      end 
    end 

    @best 
  end 

  
  # Pasa al bloque los vecinos de la  la vecindad k-opt `N(s)`. 
  #
  def each_neighbor_of(sol : Thief,k = 2)
    case k 
    when 2 then each_2opt_neighbor(sol){|n| yield n } 
    else raise "unkown k {#{k}} opt move" 
    end 
  end 

  # Itera sobre la vecindad 2opt (invertir el segmento entre dos ciudades)
  #
  # ```text 
  # 
  #  a - b - c - d - e - f- a   =>  2opt(c,f)  =>  a - b - f - e - d - c - a
  #
  # ```
  # 
  def each_2opt_neighbor(sol : Thief)
    neighbor = sol.clone 
    indexes = (1 ... @ttp.ncities).to_a 
    len = indexes.size 

    # Se elige de forma aleatoria  una  ciudad y se asegura que no 
    # se va a volver a elegir, se realiza el cambio con todas las otras 
    # ciudades, se invierte y se pasa al bloque, después se revierte el cambio 
    # para probar el siguiente vecino.
    (0 ... len).each do |k| 
      ri = rand(k ... len)
      a = indexes[ri] 
      indexes.swap(ri,k)

      ((k+1) ... len).each do |l| 
        ri = rand(l ... len)
        b = indexes[ri]
        indexes.swap(ri,l) 

        from,to = {a,b}.minmax

        neighbor.reverse!(from .. to) 
        yield neighbor     
        neighbor.reverse!(from .. to) 
      end 
    end 
  end 

end 

