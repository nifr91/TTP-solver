# Ricardo Nieto Fuentes 
# nifr91@gmail.com 

require "./local-search"
require "./bitflip-ls"
require "./opt-ls" 

# Clase que representa la búsqueda local con el framework CoSolver
# 
# En cada iteración de la búsqueda local se encuentra el óptimo Bit-flip 
# `BitFlipLS#search` después se encuentra el óptimo Opt `OptLS#search` de la 
# solución  encontrada previamente
#
# Finalmente se actualiza la solución si ha mejorado, en caso de no mejorar se 
# termina la búsqueda local
# 
# 
# 
class BitFlipOptLS < LS 
 
  # Constructor, asigna la función objetivo `@obj_fun` a las dos búsquedas 
  # locales. 
  # 
  def initialize(
    ttp      : TTP,
    duration : Time::Span,
    obj_fun  : Thief->Float64,
    logger = Logger.new)

    super(ttp,duration,obj_fun,logger)

    @bfls = BitFlipLS.new(@ttp,duration/2,@obj_fun)
    @optls = OptLS.new(@ttp,duration/2,@obj_fun)
  end
  
  # Constructor, asigna una función diferente a cada búsqueda local 
  # `bf_obj_fun` corresponde a la búsqueda Bit-Flip, `opt_obj_fun` a la 
  # búsqueda Opt y `@obj_fun` a la búsqueda BitFlip-Opt
  # 
  def initialize(
    @ttp         : TTP,
    @duration    : Time::Span,
    @obj_fun     : Thief->Float64,
    bf_obj_fun   : Thief->Float64,
    opt_obj_fun  : Thief->Float64,
    @logger      = Logger.new)

    @bfls = BitFlipLS.new(@ttp,duration/2,bf_obj_fun)
    @optls = OptLS.new(@ttp,duration/2,opt_obj_fun)
  end 


  # Función de búsqueda mientras no se cumpla el criterio de paro se
  # alterna entre una mejora del plan y una mejora del recorrido 
  #
  def search(sol : Thief) : Thief
    @best = sol.clone 
    @best_fit = @obj_fun.call(@best)
    
    search_loop do 
      @improves = false 
      span = @timer.time_left / 2
    
      @bfls.duration = span 
      @optls.duration = span

      ls_sol = @bfls.search(@best)
      ls_sol = @optls.search(ls_sol)

      ls_fit = @obj_fun.call(ls_sol)

      if ls_fit > @best_fit 
        update_best(ls_sol,ls_fit)
        @improves = true 
      end 
  
    end 

    @best 
  end 

end 
