# Ricardo Nieto Fuentes
# nifr91@gmail.com

require "../lkh/lkh.cr"
require "../local-search/bitflip-ls.cr" 
require "../local-search/opt-ls.cr"

class TTP 
  
  # Inicializaciones de la selección de objetos 
  enum PlanInit
    Bitflip
    Empty
    Exact
    Greedy
    Packiterative
    Random
  end 

  # Inicializaciones del recorrido 
  enum TourInit
    Lkh
    Nearest
    Random
  end 


  # Genera un ladrón empleando los algoritmos especificados
  #
  def init_thief(
    tour_init : TourInit, 
    plan_init : PlanInit, 
    lkh_exe = ".lkh.exe")
  
    tour = case tour_init 
      when .random?  then random_tour 
      when .nearest? then nearest_tour 
      when .lkh?     then lkh_tour(lkh_exe)
      else raise "unkown tour-init: #{tour_init}"
    end 
    
    plan = case plan_init
      when .random?        then random_plan
      when .bitflip?       then bitflip_plan(tour)
      when .packiterative? then packiterative_plan(tour)
      when .exact?         then exact_plan(tour)
      when .greedy?        then greedy_plan(tour)
      when .empty?         then empty_plan
      else raise "unkown plan-init: #{plan_init}" 
    end 


    Thief.new(tour,plan)
  end 
  
  # Genera un plan vacío en donde ningún elemento es seleccionado
  #  
  def empty_plan 
    Array.new(@nitems,false)
  end 

  # Genera un plan mediante una estrategia greedy simple 
  #  
  def greedy_plan(tour : Tour)
    plan = self.empty_plan 
    thief = Thief.new(tour,plan)

    sorted_items = @items.sort_by{|e| e.val.to_f / e.wgt.to_f }.reverse! 
    wgt = 0.0 
    
    best_fit = self.fitness(thief)

    sorted_items.each do |item| 

      if ((wgt + item.wgt) <= @mx_wgt) 
        wgt += item.wgt 
        plan[item.id] = true
        fit = self.fitness(thief) 

        if  (fit > best_fit)
          best_fit = fit 
        else 
          wgt -= item.wgt 
          plan[item.id] = false
        end 
      end  
    end 

    plan 
  end 

  # Obtiene el plan de recolección exacto, empleando programación dinámica.
  #
  # Emplea una matriz [objetos x peso]
  #
  # ```text
  #    |0 .. l .. k .. W
  #  --|-----------------
  #  0 |0   ....       0  <= considerar ningun objeto con cualquier  capacidad
  #  . |.
  #  j |.         *       <= considerar j objetos con exactamente peso k.
  #  . |.
  #  n |0    x            <= Máximo valor después de considerar todos los objetos
  #     ^                    con exactamente peso l
  #     considerar n objetos con 0 capacidad.
  # ```
  #
  # La idea es almacenar la información de la máxima ganancia al considerar
  # j objetos con exactamente el peso 'k', en función de llevar o no el objeto
  # j-esimo.
  #
  # Si se lleva el objeto j-esimo se suma la ganancia de llevarlo y se añade la
  # diferencia en tiempo que produce el llevarlo.
  #
  # Si no se lleva el objeto, la ganancia es igual a considerar j-1 objetos con
  # exactamente el peso k.
  #
  # Regresa [{[[Item]],Float}] el plan de recolección y su fitness
  #
  def exact_plan(tour : Tour) 
    
    # Obtener la distancia faltante en cada ciudad y ordenar los objetos 
    # por ciudad desde la ciudad origen hasta la última. 
    rem_dist = rem_dist_at_city(Thief.new(tour,[false]))
    sorted_items = @items.sort_by{|item| rem_dist[item.city] }.reverse!

    # Generar la matriz y el caso base (no llevar ningún objeto)
    memo_mat = Array.new(@nitems + 1){ Array.new(@mx_wgt + 1){ -1.0/0.0 } }
    memo_mat[0][0] = -@rratio * (rem_dist[tour[0]] / @mx_vel)
    (1 .. @mx_wgt).each{ |i| memo_mat[0][i] = memo_mat[0][0]} 

    # Llenar la matriz 
    (1 .. @nitems).each do |ni| 
      (0 .. @mx_wgt).each do |wgt| 
        item = sorted_items[ni - 1]
       
        if (wgt - item.wgt) < 0 
          memo_mat[ni][wgt] = memo_mat[ni - 1][wgt] 
        else 
          # Calcular la ganancia que aporta el objeto
          new_vel   = 1.0 / (@mx_vel - (@nu * wgt))
          old_vel   = 1.0 / (@mx_vel - (@nu * (wgt - item.wgt)))
          delta_vel = new_vel - old_vel
          profit    = item.val - (@rratio * (rem_dist[item.city] * delta_vel))
          

          # Elegir la máxima ganancia entre llevar o dejar el objeto 
          drop_val = memo_mat[ni - 1][wgt] 
          pick_val = memo_mat[ni - 1][wgt - item.wgt] + profit 
          memo_mat[ni][wgt] = (drop_val > pick_val)? drop_val : pick_val 
        end 
      end 
    end 

    # Buscar el peso con el fitness máximo 
    wgt = (0 .. @mx_wgt).max_by{|i| memo_mat[-1][i]}

    # memo_mat.each do |row|
    #   row.each do |col| 
    #     print col," " 
    #   end 
    #   puts 
    # end 
  
    # Generar el plan a partir de la matriz de programación dinámica 
    plan = Array.new(@nitems,false)
    @nitems.downto(1) do |row| 
      if memo_mat[row][wgt] != memo_mat[row - 1][wgt]
        item = sorted_items[row - 1] 
        wgt -= item.wgt 
        plan[item.id] = true 
      end 
    end 
    
    plan
  end 
 

  # Estrategia base para la función packiterative
  #
  # Emplea el puntaje 
  # 
  # ```text 
  #             valor^a 
  # s =  ---------------------
  #        distancia * peso^a
  # ```
  # 
  # para después de manera greedy ir agregando los objetos hasta que no 
  # quepan en la mochila o ya no se mejore. Con la idea de disminuir el costo 
  # computacional se evalua la función objetivo cada cierta cantidad de objetos
  # siendo la frecuencia de evaluación la variable `freq`
  # 
  private def pack_plan(tour : Tour,alpha : Float64)

    rem_dist = self.rem_dist_at_city(Thief.new(tour,[false])) 

    sorted_items = @items.sort_by do |item| 
      ((item.val.to_f / item.wgt.to_f)**alpha) * (1.0/rem_dist[item.city])
    end
    sorted_items.reverse!
    
    freq = (@nitems // 4).floor
    plan = Array.new(@nitems,false) 
    wgt  = 0
    plan_thief = Thief.new(tour,plan)

    best_plan = plan.dup
    best_wgt = wgt 
    best_fit  = -1.0/0.0 

    cnt = best_cnt = 0

    while(wgt < @mx_wgt) && (freq > 0) && (cnt < @nitems)
      item = sorted_items[cnt] 

      if (wgt + item.wgt) <= @mx_wgt 
        plan[item.id] = true 
        wgt += item.wgt 
        
        if (cnt % freq) == 0
          fit = self.fitness(Thief.new(tour,plan)) 
          
          if fit < best_fit 
            plan = best_plan.dup
            cnt  = best_cnt
            wgt  = best_wgt
            freq = freq // 2
          else 
            best_plan = plan.clone
            best_cnt  = cnt
            best_fit  = fit
          end 
        end 
      end 

      cnt += 1
    end 
    
    plan 
  end 

  # Algoritmo del artículo 
  # 
  # "Approximate_approaches to the travelling thief problem" 
  #                     Hayden Faulkner, Sergey Polyakobskiy
  # 
  # Se corre la rutina pack iterativamente para diferentes valores de 
  # exponente.
  # 
  def packiterative_plan(
    tour : Tour, alpha = 5.0, d = 2.5 , mx_it = 100, eps = 1e-1)
    
    pl = pack_plan(tour,alpha - d)
    l_fit = self.fitness(Thief.new(tour,pl)) 
    pr = pack_plan(tour,alpha + d)
    r_fit = self.fitness(Thief.new(tour,pr))
    
    best_plan = pack_plan(tour,alpha)  
    m_fit = self.fitness(Thief.new(tour,best_plan))

    cnt = 0 
    while cnt < mx_it 
      if (l_fit > m_fit) && (r_fit > m_fit)
        if l_fit > r_fit 
          m_fit = l_fit 
          alpha -= d 
          best_plan = pl
        else
          m_fit = r_fit 
          alpha += d 
          best_plan = pr 
        end 
      elsif l_fit > m_fit 
        m_fit = l_fit 
        alpha -= d 
        best_plan = pl 
      elsif r_fit > m_fit 
        m_fit = r_fit 
        alpha += d 
        best_plan = pr 
      end 

      d /= 2.0
      pl = pack_plan(tour,alpha - d)
      l_fit = self.fitness(Thief.new(tour,pl)) 
      pr = pack_plan(tour,alpha + d)
      r_fit = self.fitness(Thief.new(tour,pr))
      
      cnt += 1 
      break if ((l_fit - m_fit) < eps) && ((r_fit - m_fit) < eps) 
    end 

    best_plan
  end 


  # Genera un plan a partir de un recorrido, empleando una búsqueda local 
  # con vecindad bitflip, partiendo de un plan de recolección vacío
  #
  def bitflip_plan(tour : Tour, plan =  self.empty_plan ) 
    thief = Thief.new(tour,plan) 
    bfls = BitFlipLS.new(self,3600.seconds,->self.fitness(Thief))
    thief = bfls.search(thief) 
    thief.plan
  end 

  # Genera un plan de recolección aleatoriamente, elige un valor de capacidad
  # de forma aleatoria en el rango 0 .. @mx_wgt e intenta agregar objetos 
  # de manera aleatoria mientras quepan en la nueva capacidad. 
  #
  def random_plan
    plan   = Array.new(@nitems,false)
    max_wgt = rand(1.0) * @mx_wgt 
    
    wgt = 0.0 
    (0 ... @nitems).to_a.shuffle!.each do |id| 
      if wgt + @items[id].wgt <= max_wgt
        plan[id] = true
        wgt += @items[id].wgt
      end 
      break if wgt > max_wgt 
    end 

    plan 
  end 

  # Genera un recorrido aleatorio 
  #
  def random_tour
    tour = (0 ... @ncities).to_a
    (1 ... @ncities).each{|i| tour.swap(i,rand(i ... @ncities) ) }
    tour
  end 


  # Genera un recorrido empleando la librería LKH 
  #
  def lkh_tour(lkh_exe : String)
    LKH.tour(self,lkh_exe)
  end 


  # Genera un recorrido empleando la estrategia greedy del vecino mas 
  # cercano 
  # 
  def nearest_tour(k=1)
    tour = [0]
    
    cities = (1 ... @ncities).to_a 
        
    until cities.empty? 
      cities.sort_by!{|id| dist(tour[-1],id)}
      index = rand({1,{cities.size,k}.min}.max) 
      tour.push cities.swap(index,-1).pop 
    end  
    tour
  end 

end 
