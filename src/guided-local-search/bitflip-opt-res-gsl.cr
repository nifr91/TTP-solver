require "./bitflip-opt-gls.cr" 
require "../utils/penalization.cr" 

# Clase que representa la búsqueda local guiada con estrategia de reinicio 
#
# Se realiza la búsqueda local guiada en un intervalo de tiempo determinado, en 
# caso de no existir mejora de la solución en ese intervalo con respecto a la 
# anterior se realiza un reinicio. 
#
# En cada iteración se actualiza la solución si se ha mejorado.
# 
class BitFlipOptResGLS < BitFlipOptGLS 
 
  # Constructor que asigna el tiempo de reinicio y la ruta al archivo de la 
  # librería LKH. 
  # 
  def initialize(
    ttp          : TTP ,
    duration     : Time::Span,
    obj_fun      : Thief -> Float64,
    @restart_span : Time::Span, 
    @lkh_exe      : String, 
    logger = Logger.new) 

    super(ttp,duration,obj_fun,logger)
    @restart_timer = Timer.new(@restart_span)

    @bfls = BitFlipLS.new(@ttp,duration/2,@obj_fun)
    @optls = OptLS.new(@ttp,duration/2,->penalized_obj_fun(Thief))
  end 
  
  # Función de búsqueda, mientras no se cumpla el criterio de paro, se realiza 
  # una búsqueda local guiada por el tiempo de reinicio especificado, si se 
  # encuentra una mejora se mantienen las penalizaciones, en caso de no
  # encontrarse mejora se realiza un reinicio.
  #
  def search(sol : Thief) 
    @best = sol.clone 
    @best_fit = @obj_fun.call(@best) 
    
    gls_sol = @best 
    gls_fit = @best_fit 

    @penalty.reset 
    @lambda = 0.5 * (@best_fit / @ttp.ncities).abs 
    restart_improvement = false
    @restart_timer = Timer.new(@restart_span)

    search_loop do 
     
      if @restart_timer.finished?
        @restart_timer = Timer.new(@restart_span)

        unless restart_improvement 
          gls_sol = 
            @ttp.init_thief(TTP::TourInit::Lkh,TTP::PlanInit::Greedy,@lkh_exe)
          gls_fit = @obj_fun.call(gls_sol)
          @penalty.reset
          @logger.res_cnt += 1
        end 

        restart_improvement = false
      end  

      loop do 
        span = {@restart_timer.time_left,@timer.time_left}.min / 2
        @bfls.duration = span 
        @optls.duration = span  

        gls_sol = @optls.search(gls_sol)
        gls_sol = @bfls.search(gls_sol)
        
        prev_fit = gls_fit 
        gls_fit = @obj_fun.call(gls_sol) 
        restart_improvement = true if ((gls_fit - prev_fit) > 1)
       


        break if prev_fit == gls_fit || @restart_timer.finished? 
      end 

      update_best(gls_sol,gls_fit) if gls_fit > @best_fit

      @penalty.update(gls_sol)
    end 
    @logger.solution(@best,@best_fit)
    @best
  end 

end


class TourPenalization
  getter ary
end 

