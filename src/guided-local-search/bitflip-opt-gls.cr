# Ricardo Nieto Fuentes
# nifr91@gmail.com 

require "../local-search/bitflip-opt-ls.cr"
require "../utils/penalization.cr"

# Clase que representa la búsqueda local guiada empleando el framework CoSolver 
# a través de la búsqueda local `BitFlioOptLS`.
#
# En cada iteración se realiza una búsqueda local bitflip-opt y se actualiza la 
# penalización del óptimo 
#
# La idea es forzar la exploración mediante el recorrido, obteniendo el óptimo 
# local y penalizándolo para salir de la zona de atracción y moverse a otra 
# región del espacio de búsqueda.
#
class BitFlipOptGLS < LS 
  
  # Constructor, asigna la función objetivo penalizada a la búsqueda local del 
  # recorrido y la `@obj_fun` original a la búsqueda local del plan. 
  #  
  def initialize(
    ttp,
    duration,
    obj_fun,
    logger  = Logger.new)
    super(ttp,duration,obj_fun,logger) 
    
    @lambda = 0.0

    @penalty = TourPenalization.new(@ttp)

    @localsearch = BitFlipOptLS.new(
      @ttp, @duration, obj_fun, obj_fun,->penalized_obj_fun(Thief)) 
  end

  # Función objetivo penalizada por la frecuencia de aparición de las 
  # características presentas en las soluciones óptimas.
  # 
  def penalized_obj_fun( sol : Thief)
    @obj_fun.call(sol) - (@lambda * @penalty.call(sol).to_f) 
  end 

  # Función de búsqueda, la cual se realiza hasta que se termine el tiempo 
  # especificado, en cada iteración se realiza una búsqueda local 
  # `BitFlipOptLS#search` y se penaliza sobre las aristas del óptimo encontrado
  # 
  def search(sol : Thief)  : Thief
    @best = sol.clone 
    @best_fit = @obj_fun.call(@best)
    gls_sol = @best 
    gls_fit = @best_fit 

    @penalty.reset  
    @lambda = 0.5 * (@best_fit / @ttp.ncities.to_f).abs 

    search_loop do 
      
      @localsearch.duration = @timer.time_left 
      gls_sol = @localsearch.search(gls_sol) 
      
      gls_fit = @obj_fun.call(gls_sol)

      if gls_fit > @best_fit 
        update_best(gls_sol,gls_fit)
      end 
        
      @penalty.update(gls_sol)
    end 

    @best  
  end 
end 


