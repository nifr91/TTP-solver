require "./spec-helper" 
require "tempfile"

describe "Logger" do 
 
  describe "new" do 
    it "creates an object with default values" do 
      log = Logger.new 
      log.span.should eq 1.seconds
    end

    it "creates an objects and passes it to a block for config" do 
      
      logfile = Tempfile.new("log-log") 
      evofile = Tempfile.new("evo-log")
      solfile = Tempfile.new("sol-log")

      log = Logger.new do |log| 
        log.iolog = logfile 
        log.ioevo = evofile 
        log.iosol = solfile  
        log.span = 10.seconds
      end 

      log.iolog.should eq logfile
      log.ioevo.should eq evofile  
      log.iosol.should eq solfile
      log.span.should eq 10.seconds
      
      evofile.unlink
      solfile.unlink 
      logfile.unlink
    end 

  end 

  describe "setters" do 
    io = IO::Memory.new 
    log = Logger.new 

    describe "iolog=" do 
      it "captures the log io" do 
        log.iolog.should_not be io 
        log.iolog = io 
        log.iolog.should be io 
      end 
    end

    describe "ioevo=" do 
      it "captures the evo io" do 
        log.ioevo.should_not be io 
        log.ioevo = io 
        log.ioevo.should be io 
      end 
    end

    describe "iosol=" do 
      it "captures the evo io" do 
        log.iosol.should_not be io 
        log.iosol = io 
        log.iosol.should be io 
      end 
    end
  end 



  describe "measure?" do 
    it "returns true if from last log has passed at least 'span' time"  do 
      io = IO::Memory.new 

      log = Logger.new do |log| 
        log.span = 0.1.seconds 
        log.ioevo  = io
      end 
      
      log.measure?.should be_false 
      sleep(0.1)
      log.measure?.should be_true 
      
      sleep(0.1)
      log.measure?.should be_true 

      log.evolution(Thief.new,1.0)
      log.measure?.should be_false
    end 
  end 

  describe "solution" do 
    sol = Thief.new([0,1],[false]) 

    iosol = IO::Memory.new 
    iosol.puts "# Header" 


    io = IO::Memory.new 
    io.puts "# Header"
    io.puts "# Instance"
    io.puts "ttp-inst"
    io.puts "# Tour"
    io.puts sol.tour_str
    io.puts "# Plan"
    io.puts sol.plan_str 
    io.puts "# Fitness"
    io.puts "1.0"

    log = Logger.new do |log| 
      log.iosol = iosol
    end 

    log.solution("ttp-inst",sol,"1.0")
    
    log.iosol.to_s.should eq io.to_s
  end 
 
  describe "evolution" do 

    pop = [Thief.new([0,1,2],[false]),Thief.new([0,2,1],[true])]
    fit = [1.0,2.0]

    it "accepts a single solution and fitness" do 
      
      log = Logger.new do |log|
        log.ioevo = IO::Memory.new 
      end 
     
      log.evolution(pop[0],fit[0],true)
      
      lines = log.ioevo.to_s.each_line.to_a  
      lines.size.should eq 1 
      
      vals = lines[0].split(" ")
      
      vals[0].to_f.should be_close 0.0,1e-2
      vals[1].to_f.should eq 1.0 
      vals[2].should eq pop[0].tour_str 
      vals[3].should eq pop[0].plan_str 
      vals[4].to_i.should eq 0 
      vals[5].to_i.should eq 0

    end 

    it "only logs if measure? returns true " do 
      log = Logger.new do |log| 
        log.ioevo = IO::Memory.new 
        log.span = 0.1.seconds 
      end 
        
      log.evolution(pop[0],fit[1])
      log.ioevo.to_s.empty?.should be_true 
      sleep(0.1)
      log.evolution(pop[0],fit[1])
      log.ioevo.to_s.empty?.should be_false 
    end

    it "accepts a population of solutions " do 
      log = Logger.new do |log| 
        log.ioevo = IO::Memory.new 
      end 
      
      log.evolution(pop,fit,true)
      
      lines = log.ioevo.to_s.each_line.to_a 
      lines.size.should eq 3

      vals = lines[0].split(" ")
      vals.size.should eq 3
      vals[0].to_f.should be_close 0.0,1e-2 
      vals[1].to_f.should eq 2.0 
      vals[2].to_i.should eq 0 


      lines[1].should eq "1.0 1,2,3 0"
      lines[2].should eq "2.0 1,3,2 1"
      

    end  
  end  

  describe "log" do 
    it "writes the passed string into the logfile" do 
      
      log = Logger.new(&.iolog = IO::Memory.new) 
      log.log "test"
      
      lines = log.iolog.to_s.each_line.to_a
      lines.size.should eq 2
      lines[0].starts_with?("# ").should be_true
      lines[0].gsub("# ","").to_f.should be_close 0.0,1e-2
      lines[1].should eq "test" 

    end 
  end

  describe "close" do 
    it "closes all the ios of the logger" do

      log = Logger.new do |log| 
        log.ioevo = IO::Memory.new 
        log.iosol = IO::Memory.new 
        log.iolog = IO::Memory.new 
      end 

      log.close 
      log.ioevo.closed?.should be_true 
      log.iosol.closed?.should be_true 
      log.iolog.closed?.should be_true

    end 
  end  
end 
