# Ricardo Nieto Fuentes
# nifr91@gmail.com

require "./spec-helper"

describe "LKH" do 
  

  describe "tour" do
    tour = LKH.tour(Eil51::INST,Files::LKH_EXE)
    it "creates a valid lkh-tour using the LKH library" do 
      Eil51::INST.validate_tour(Thief.new(tour,[false]).tour_str).should eq TTP::ThiefError::None
    end 
  end 
  
  describe "load_tour" do 
    it "loads a lkh-tour" do 
      tour = LKH.load_tour(Files::LKH_TOUR)
      tour.should eq [0, 21, 1, 15, 49, 8, 29, 33, 20, 28, 19, 34, 35, 2, 27, 30, 7, 25, 6, 42, 23, 22, 47, 5, 26, 50, 45, 11, 46, 3, 17, 13, 24, 12, 40, 39, 18, 41, 43, 16, 36, 14, 44, 32, 38, 9, 48, 4, 37, 10, 31]
    end 
  end 


   describe "tour_file" do 
    it "creates a file that will contain the tour" do 
      tour_file = LKH.tour_file 
      File.exists?(tour_file.path)
      tour_file.unlink
    end   
  end 

  describe "tsp_file" do 
    tsp_file = LKH.tsp_file(Eil51::INST)

    it "creates a tsp instance file for the LKH library executable" do 
      File.exists?(tsp_file.path).should be_true 
    end 
  
    it "is a valid tsp file" do 
      lines = File.read(tsp_file.path).each_line.map(&.strip).to_a

      lines[0].should eq "NAME : #{Eil51::NAME}" 
      lines[1].should eq "COMMENT : No comment"
      lines[2].should eq "TYPE : TSP"
      lines[3].should eq "DIMENSION : #{Eil51::NCITIES}"
      lines[4].should eq "EDGE_WEIGHT_TYPE : CEIL_2D"
      lines[5].should eq "NODE_COORD_SECTION"
      
      (lines.size - 6).should eq Eil51::NCITIES
      
      Eil51::NCITIES.times do |i|
        city = Eil51::CITIES[i] 
        lines[i+6].should eq "#{city.id + 1} #{city.x.to_i} #{city.y.to_i}"
      end 
    
    end   
  
    it "creates a different file each time its called" do 
      new_tsp_file = LKH.tsp_file(Eil51::INST)
      File.basename(new_tsp_file.path).should_not eq File.basename(tsp_file.path)
      new_tsp_file.unlink
    end 

    tsp_file.unlink 
  end 


  describe "Params" do 
    describe "new" do 
      it "receives a block to configure the parameters" do 
        params = LKH::Params.new do |params| 
          params.tsp_filename       = "/temp/tsp.tsp"
          params.tour_filename      = "/temp/tour.tour"
          params.move_type          = 5
          params.time_limit         = 5.0
          params.seed               = 0
          params.trace_level        = 0
          params.max_swaps          = Eil51::NCITIES
          params.max_trials         = Eil51::NCITIES
          params.max_candidates     = 5
          params.candidate_set_type = "N"
        end

        params.tsp_filename.should       eq "/temp/tsp.tsp"
        params.tour_filename.should      eq "/temp/tour.tour"
        params.move_type.should          eq 5
        params.time_limit.should         eq 5.0
        params.seed.should               eq 0
        params.trace_level.should        eq 0
        params.max_swaps.should          eq Eil51::NCITIES
        params.max_trials.should         eq Eil51::NCITIES
        params.max_candidates.should     eq 5
        params.candidate_set_type.should eq "N"
        
      end 
    end 
  end 

  describe "parameters_file" do 
    tsp_file = LKH.tsp_file(Eil51::INST)
    tour_file = LKH.tour_file 
    params = LKH::Params.new do |params| 
      params.tsp_filename       = tsp_file.path
      params.tour_filename      = tour_file.path
      params.move_type          = 5
      params.time_limit         = 5.0
      params.runs               = 1
      params.seed               = rand(Int32::MAX)
      params.trace_level        = 0
      params.max_swaps          = Eil51::NCITIES
      params.max_trials         = Eil51::NCITIES
      params.max_candidates     = 5
      params.candidate_set_type = "N"
    end


    params_file = LKH.params_file(params)

    it "creates a parameters file for LKH library executable" do 
      File.exists?(params_file.path).should be_true 
    end 

    it "creates a valid parameters file" do 
      lines = File.read_lines(params_file.path)
      
      lines.size.should eq 11

      lines[0].should  eq "PROBLEM_FILE = #{tsp_file.path}"
      lines[1].should  eq "OUTPUT_TOUR_FILE = #{tour_file.path}"
      lines[2].should  eq "MOVE_TYPE = 5"
      lines[3].should  eq "TIME_LIMIT = 5.0"
      lines[4].should  eq "RUNS = 1"
      lines[5].should  eq "TRACE_LEVEL = 0"
      lines[6].should  eq "SEED = #{params.seed}"
      lines[7].should  eq "MAX_SWAPS = #{Eil51::NCITIES}"
      lines[8].should  eq "MAX_TRIALS = #{Eil51::NCITIES}"
      lines[9].should  eq "MAX_CANDIDATES = 5"
      lines[10].should eq "CANDIDATE_SET_TYPE = N"

    end 

    it "creates a different file each time" do 
      new_param_file = LKH.params_file(params)
      File.basename(new_param_file.path).should_not eq File.basename(params_file.path)
      new_param_file.unlink
    end 

    tsp_file.unlink
    tour_file.unlink 
    params_file.unlink 

  end 

  
end 
