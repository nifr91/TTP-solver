require "./spec-helper" 

class BitFlipOptLS < LS 
  getter bfls 
  getter optls
  getter ttp
end

class LS 
  getter obj_fun
end 

describe "BitFlipOptLS" do 
  
  bfoptls = BitFlipOptLS.new(Eil51::INST,0.1.seconds,->Eil51::INST.fitness(Thief))

  describe "new" do 
    it "accepts one objective function" do 
      temp = BitFlipOptLS.new(
        Eil51::INST,1.seconds,->Eil51::INST.fitness(Thief))

      temp.obj_fun.should eq ->Eil51::INST.fitness(Thief)
      temp.bfls.obj_fun.should eq ->Eil51::INST.fitness(Thief)
      temp.optls.obj_fun.should eq ->Eil51::INST.fitness(Thief)
      temp.duration.should eq 1.seconds 
      temp.ttp.should be Eil51::INST 
    end
    
    it "accepts three objective functions" do 
      a = ->(s : Thief){1.0} 
      b = ->(s : Thief){2.0}
      c = ->(s : Thief){3.0}

      temp = BitFlipOptLS.new(Eil51::INST,1.seconds,a,b,c)

      temp.obj_fun.should eq a
      temp.bfls.obj_fun.should eq b
      temp.optls.obj_fun.should eq c 
      temp.duration.should eq 1.seconds 
      temp.ttp.should be Eil51::INST 

    end 
  end 

  describe_search(bfoptls)  

  describe "search" do 
    it "terminates when it cannot improve more" do 
      bfoptls.duration = 1.seconds
      tic = Time.monotonic  
      bfoptls.search(Eil51::ASOL)
      (Time.monotonic - tic).to_f.should_not be_close 1,1e-1
    end 
  end 
end 
