require "./spec-helper" 


describe "TTP" do 
  
  describe "new" do 
    items = [Item.new(0,10,15,1), Item.new(1,11,6,2)]
    cities = [City.new(0,0.0,0.0),City.new(1,1.0,0.0),City.new(2,0.0,1.0)]

    dist = [ 
      [ City.euc_dist(cities[0],cities[0]),
        City.euc_dist(cities[0],cities[1]),
        City.euc_dist(cities[0],cities[2]) ],
      [ City.euc_dist(cities[1],cities[0]),
        City.euc_dist(cities[1],cities[1]),
        City.euc_dist(cities[1],cities[2]) ],
      [ City.euc_dist(cities[2],cities[0]),
        City.euc_dist(cities[2],cities[1]),
        City.euc_dist(cities[2],cities[2]) ]
    ]
    
    it "creates a new instance" do 
      ttp_inst = TTP.new(
        name:      "ttp-inst",
        kp_type:   "uncorr",
        ncities:   cities.size,
        nitems:    items.size,
        mx_wgt:    20,
        mx_vel:    1.0,
        mn_vel:    0.1,
        rratio:    1.0,
        edge_type: "CEIL_2D",
        cities:    cities.clone,
        items:     items.clone, 
        dist:      dist)

      ttp_inst.name.should      eq "ttp-inst"
      ttp_inst.kp_type.should   eq "uncorr"
      ttp_inst.ncities.should   eq 3
      ttp_inst.nitems.should    eq 2
      ttp_inst.mx_wgt.should    eq 20
      ttp_inst.mx_vel.should    eq 1.0
      ttp_inst.mn_vel.should    eq 0.1
      ttp_inst.rratio.should    eq 1.0
      ttp_inst.edge_type.should eq "CEIL_2D"
      ttp_inst.cities.should    eq cities
      ttp_inst.items.should     eq items
      ttp_inst.dist.should      eq dist
      ttp_inst.nu.should        eq (1.0 - 0.1)/20.0
    end 
  end 




  # IO ------------------------------------------------------------------------
  describe "IO" do 
    describe "self.load" do 
      it "creates a instance object from file" do 
        ttp_inst = TTP.load Files::EIL51 

        ttp_inst.name.should      eq Eil51::NAME     
        ttp_inst.kp_type.should   eq Eil51::KP_TYPE  
        ttp_inst.ncities.should   eq Eil51::NCITIES  
        ttp_inst.nitems.should    eq Eil51::NITEMS   
        ttp_inst.mx_wgt.should    eq Eil51::MX_WGT   
        ttp_inst.mx_vel.should    eq Eil51::MX_VEL   
        ttp_inst.mn_vel.should    eq Eil51::MN_VEL   
        ttp_inst.rratio.should    eq Eil51::RRATIO   
        ttp_inst.edge_type.should eq Eil51::EDGE_TYPE
        ttp_inst.cities.should    eq Eil51::CITIES
        ttp_inst.items.should     eq Eil51::ITEMS
        ttp_inst.dist.should      eq Eil51::DIST
        ttp_inst.nu.should        eq Eil51::NU

      end 
    end 
  end  

  # Evaluation ----------------------------------------------------------------
  describe "Evaluation" do 

    describe "fitness" do 
      it "Calculates the fitness of the given solution" do
        inst = Eil51::INST 
        inst.fitness(Eil51::ASOL).should be_close Eil51::ASOL_FIT,1e-10
        inst.fitness(Eil51::BSOL).should be_close Eil51::BSOL_FIT,1e-10
      end  
    end 
    
    describe "profit" do 
      it "Calculates the total profit of the selected items" do 
        inst = Eil51::INST 
        inst.profit(Eil51::ASOL.plan).should be_close Eil51::ASOL_PROFIT,1e-10
        inst.profit(Eil51::BSOL.plan).should be_close Eil51::BSOL_PROFIT,1e-10
      end 
    end 

    describe "cities_wgts" do 
      it "Calculates the total weight of items in each city" do 
        inst = Eil51::INST 
        inst.cities_wgt(Eil51::ASOL.plan).should eq Eil51::ASOL_CITIES_WGT
        inst.cities_wgt(Eil51::BSOL.plan).should eq Eil51::BSOL_CITIES_WGT
      end 
    end   
    
    describe "acc_wgt" do 
      it "Calculates the accumulated weight at ith-step in tour " do 
        inst = Eil51::INST 
        inst.acc_wgt(Eil51::ASOL).should eq Eil51::ASOL_ACC_WGT
        inst.acc_wgt(Eil51::BSOL).should eq Eil51::BSOL_ACC_WGT
      end 
    end 
    
    describe "time" do 
      it "Calculates the total time to travel the tour" do 
        inst = Eil51::INST 
        inst.fitness(Eil51::ASOL).should be_close Eil51::ASOL_FIT,1e-10
        inst.fitness(Eil51::BSOL).should be_close Eil51::BSOL_FIT,1e-10
      end 
    end 
    
    describe "wgt" do 
      it "Gives the total weight of the picked items" do 
        inst = Eil51::INST 
        inst.wgt(Eil51::ASOL).should eq Eil51::ASOL_ACC_WGT[-1]
        inst.wgt(Eil51::BSOL).should eq Eil51::BSOL_ACC_WGT[-1]
      end 
    end 
    
    describe "distance" do 
      it "Gives the total distance of the tour" do 
        inst = Eil51::INST 
        inst.distance(Eil51::ASOL).should be_close Eil51::ASOL_DIST,1e-10
        inst.distance(Eil51::BSOL).should be_close Eil51::BSOL_DIST,1e-10
      end 
    end 

    describe "rem_dist" do 
      it "Returns an array with the remaining distance at each step in tour" do 
        inst = Eil51::INST 

        ary = inst.rem_dist(Eil51::ASOL)
        (0 ... inst.ncities).each do |i|
          ary[i].should be_close Eil51::ASOL_REM_DIST[i],1e-10  
        end  

        ary = inst.rem_dist(Eil51::BSOL)
        (0 ... inst.ncities).each do |i|
          ary[i].should be_close Eil51::BSOL_REM_DIST[i],1e-10  
        end  
      end 
    end 

  end 
  

  # Solution Init -------------------------------------------------------------

  describe "sol-init" do 
    inst  = Eil51::INST 

    describe "build_thief" do 
      it "returns a valid thief for all initializations" do 
        TTP::PlanInit.each do |pi|
          TTP::TourInit.each do |ti|
            thief = Eil51::INST.init_thief(ti,pi,lkh_exe: Files::LKH_EXE)
            inst.validate_thief(thief.to_s).should eq TTP::ThiefError::None
          end 
        end 
      end 
    end 

    describe "empty_plan" do 
      empty_plan_thief = Thief.new( [] of Int32,inst.empty_plan)

      it "returns a valid plan" do 
        inst.validate_plan(empty_plan_thief.plan_str).should eq TTP::ThiefError::None
      end 

      it "creates a plan where no item is selected" do 
        empty_plan_thief.plan.should eq Array.new(Eil51::NITEMS,false)
      end
    end 

    describe "greedy_plan" do
      tour = Eil51::BSOL.tour 
      greedy_plan_thief = Thief.new(tour,inst.greedy_plan(tour))
      
      it "returns a valid plan" do 
        inst.validate_plan(greedy_plan_thief.plan_str).should eq TTP::ThiefError::None
      end 
      
      it "creates the same plan given the same tour" do 
        inst.greedy_plan(tour).should eq greedy_plan_thief.plan
      end 

      it "creates a non empty plan" do 
        inst.greedy_plan(tour).should_not eq inst.empty_plan 
      end 
    end 

    describe "nearest_tour" do 
      nearest_thief = Thief.new(inst.nearest_tour,[] of Bool)
      
      it "returns a valid tour" do 
        inst.validate_tour(nearest_thief.tour_str).should eq TTP::ThiefError::None
      end 
      
      it "always creates the same tour" do 
        inst.nearest_tour.should eq nearest_thief.tour
      end 
    end 

    describe "lkh_tour" do 
      lkh_thief = Thief.new(inst.lkh_tour(Files::LKH_EXE),[true])
      it "resturns a valid tour" do 
        inst.validate_tour(lkh_thief.tour_str).should eq TTP::ThiefError::None
      end 
    end 
  end 


  # Validation ----------------------------------------------------------------

  describe "Validation" do 

    inst = Eil51::INST
    good_tour_str,good_plan_str = Eil51::ASOL.to_s.split(" ")

    items_mismatch_down  = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
    items_mismatch_up    = "0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
    exceeded_wgt         = "1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1"
    invalid_items        = "1,2,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0,0"
    plan_all_errors      = "1,2,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1,1"
 
    cities_mismatch_down = "1,2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50"
    repeated_cities      = "1,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51"
    invalid_cities       = "1,-2,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51"
    invalid_origin       = "2,1,3,4,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50,51"
    tour_all_errors      = "2,1,1,-1,5,6,7,8,9,10,11,12,13,14,15,16,17,18,19,20,21,22,23,24,25,26,27,28,29,30,31,32,33,34,35,36,37,38,39,40,41,42,43,44,45,46,47,48,49,50"

    thief_all_errors = "#{tour_all_errors} #{plan_all_errors}"

    describe "validate_plan" do 
      
      it "should return no error if the solution is correct" do
        inst.validate_plan(good_plan_str).should eq TTP::ThiefError::None 
      end 
      
      it "returns error if instance and packing plan differs in # of items " do 
        inst.validate_plan(items_mismatch_up).should   eq TTP::ThiefError::Items_mismatch
        inst.validate_plan(items_mismatch_down).should eq TTP::ThiefError::Items_mismatch
      end  

      it "returns error if the packing plan exceed the capacity" do 
        inst.validate_plan(exceeded_wgt).should eq TTP::ThiefError::Exceeded_weight
      end 
      
      it "return error if the plan representation is invalid" do 
        inst.validate_plan(invalid_items).should eq TTP::ThiefError::Invalid_items 
      end 
    
      it "returns the combination of errors" do
        inst.validate_plan(plan_all_errors).items_mismatch?.should  be_true
        inst.validate_plan(plan_all_errors).exceeded_weight?.should be_true
        inst.validate_plan(plan_all_errors).invalid_items?.should   be_true
      end 
    end 


    describe  "validate_tour" do 
      
      it "returns no error if the tour is correct" do 
        inst.validate_tour(good_tour_str).should eq TTP::ThiefError::None  
      end 
      
      it "returns error if instance and tour differs in # of cities" do 
        inst.validate_tour(cities_mismatch_down).should eq TTP::ThiefError::Cities_mismatch
      end 
  
      it "returns error if there are repeated cities" do 
        inst.validate_tour(repeated_cities).should eq TTP::ThiefError::Repeated_cities
      end 

      it "returns error if the tour representation is invalid" do 
        inst.validate_tour(invalid_cities).should eq TTP::ThiefError::Invalid_cities
      end 
      
      it "returns error if the tour does not start in the first city '1'" do 
        inst.validate_tour(invalid_origin).should eq TTP::ThiefError::Invalid_origin
      end 
      
      it "returns the combination of errors" do 
        inst.validate_tour(tour_all_errors).cities_mismatch?.should be_true 
        inst.validate_tour(tour_all_errors).repeated_cities?.should be_true
        inst.validate_tour(tour_all_errors).invalid_cities?.should  be_true 
        inst.validate_tour(tour_all_errors).invalid_origin?.should  be_true 
      end 

    end 
  
    describe "validate_thief" do 
      it "returns no error if is a valid solution" do 
        inst.validate_thief(Eil51::ASOL.to_s).should eq TTP::ThiefError::None
      end 
      
      it "returns all the errors found in tour and plan" do 
        inst.validate_thief(thief_all_errors).repeated_cities?.should be_true
        inst.validate_thief(thief_all_errors).cities_mismatch?.should be_true
        inst.validate_thief(thief_all_errors).invalid_cities?.should  be_true
        inst.validate_thief(thief_all_errors).invalid_origin?.should  be_true
        inst.validate_thief(thief_all_errors).items_mismatch?.should  be_true
        inst.validate_thief(thief_all_errors).exceeded_weight?.should be_true
        inst.validate_thief(thief_all_errors).invalid_items?.should   be_true
      end
    end 

    describe "validate_fitness" do 
      it "returns no error if the given value is close to the solution's" do 
        error = inst.validate_fitness(Eil51::ASOL_FIT.to_s,Eil51::ASOL.to_s) 
        error.should eq TTP::ThiefError::None         
        
        error = inst.validate_fitness((Eil51::ASOL_FIT - 1e-4).to_s,Eil51::ASOL.to_s) 
        error.should eq TTP::ThiefError::None 
 
      end 
      
      it "returns error if the given value is different form the solution's" do 
        error = inst.validate_fitness("0.0",Eil51::ASOL.to_s) 
        error.should eq TTP::ThiefError::Fitness_mismatch   
      end  
    end 
  end 

end 
