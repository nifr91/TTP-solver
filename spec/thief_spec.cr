require "./spec-helper" 

describe "Thief" do 

  describe "new" do 
    it "creates with empty values" do 
      thief = Thief.new
      thief.tour.empty?.should be_true 
      thief.plan.empty?.should be_true 
    end   
    
    it "captures the argumentes" do 
      tour = [0,1,2,3]
      plan = [true,true,true,false]

      thief = Thief.new(tour,plan)

      thief.tour.should be(tour)
      thief.plan.should be(plan) 
    end 

    it "parses a string" do 
      str = "1,2,3,4,5 0,1,1,0"
      thief = Thief.new(str) 
        
      thief.tour.should eq [0,1,2,3,4]
      thief.plan.should eq [false,true,true,false]

      str = "1 2 3 4 5 | 0 1 1 0"
      thief = Thief.new(str,"|"," ") 
        
      thief.tour.should eq [0,1,2,3,4]
      thief.plan.should eq [false,true,true,false]
    end 
  end 
  
  describe "clone" do 
    it "creates a deep copy" do 
      thief = Thief.new([0,1,2,3],[true])
      clone = thief.clone 

      thief.should_not be clone 

      thief.plan.should_not be clone.plan 
      thief.tour.should_not be clone.tour 

      thief.plan.should eq clone.plan 
      thief.tour.should eq clone.tour
    end 
  end  

  describe "==" do       
    a = Thief.new([0,1,2,3],[true,false])
    b = Thief.new([0,1,2,3],[true,false])
    c = Thief.new([2,2,1,0],[false,true])
    
    it "compares other" do 
      (a == b).should be_true 
      (a == c).should be_false 
      (b == c).should be_false 
    end 

    it "compares elements" do 
      a.tour.should     eq [0,1,2,3]
      a.tour.should_not eq [3,2,1,0]
      a.plan.should     eq [true,false]
      a.plan.should_not eq [false,true]
    end 
  end 

  describe "reverse!" do 
    thief = Thief.new([0,1,2,3,4],[true])

    it "raises when swapping the origin" do 
      expect_raises(Exception,/reverting the origin \w+/) do 
        thief.reverse!(0 .. 4)
      end 
    end

    it "inverts the segment defined by the range" do 
      thief.reverse!(1 .. 3).tour.should eq [0,3,2,1,4] 
      thief.reverse!(1 ... thief.tour.size)
      thief.tour.should eq [0,4,1,2,3]
    end 
  end 

  # IO ------------------------------------------------------------------------

  describe "I/O" do 
    thief = Thief.new([0,1,2,3],[true,true,false,false])
    plan_str = "1,1,0,0"
    tour_str = "1,2,3,4"
    thief_str = "#{tour_str} #{plan_str}"  
      

    describe "plan_str" do 
      it "returns a string that represents the plan" do 
        thief.plan_str.should eq plan_str
      end 
    end

    describe "tour_str" do 
      it "returns a string that represents the tour" do 
        thief.tour_str.should eq tour_str
      end 
    end  

    describe "to_s" do 
      it "returns a string that represents the object" do 
        thief.to_s.should eq thief_str 
      end 

      it "prints into an io a string that represents the object" do 
        io = IO::Memory.new 
        io << thief 
        io.to_s.should eq thief_str 
      end
    end 
    
    describe "inspect" do 
      it "same as to_s" do 
        thief.inspect.should eq thief_str
      end 
    end
  end  
  
  # Iterators -----------------------------------------------------------------

  describe "Iterators" do 
    thief = Thief.new([0,2,1,3],[true,true,false,true,false])
    edges = [{0,2},{2,1},{1,3},{3,0}]
    items_ids = [0,1,3]

    describe "edges" do 
      it "iterates over de edges of the tour" do 
        
        iter = thief.edges

        edges.each do |edge| 
          iter.next.should eq edge     
        end
      
        iter.next.should eq Iterator.stop

        iter.rewind
        iter.next.should eq edges[0]

      end 
    end 
  
    describe "item_ids" do 
      it "iterates over the ids of the picked items" do 
        iter = thief.items_ids

        items_ids.each do |id| 
          iter.next.should eq id 
        end 
        iter.next.should eq Iterator.stop
        
        iter.rewind 
        iter.next.should eq items_ids[0]
      end 
    end 
    
  end 

end 
