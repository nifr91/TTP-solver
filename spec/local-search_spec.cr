require "./spec-helper"

class DummyLocalSearch < LS 
  
  # Added for function testing 
  property improves 
  property timer 
  property logger 
  property best 
  property best_fit 
  property obj_fun
  property ttp
  # ---------------------------

  def search(sol : Thief)
    @best = sol.clone 
    @best 
  end
end 

describe "LocalSearch" do 
  
  obj_fun = ->(s : Thief){0.0}  
  
  logger = Logger.new do |logger| 
    logger.ioevo = IO::Memory.new
    logger.span = 0.1.seconds 
  end  

  ls = DummyLocalSearch.new(Eil51::INST,0.5.seconds,obj_fun,logger)

  describe "search" do 
    it "should be implemented and returns a solution equal or better" do 
      sol = ls.search(Eil51::ASOL)
      Eil51::INST.validate_thief(sol.to_s).should eq TTP::ThiefError::None
      Eil51::INST.fitness(sol).should be >= Eil51::INST.fitness(Eil51::ASOL)
    end  
  end 

  describe "new" do 
    it "creates a new object" do

      temp = DummyLocalSearch.new(
        Eil51::INST,1.seconds,obj_fun,Logger.new)
      
      temp.should_not be_nil 
      temp.should_not be ls

      temp.ttp.should be Eil51::INST

      temp.duration.should eq 1.seconds
      temp.best.should     eq Thief.new
      temp.best_fit.should eq -1.0/0.0
      temp.obj_fun.should  eq obj_fun 
    end 
  end 

  describe "stop_criterion" do 
    it "returns true if there's no improvement" do 
      ls.timer = Timer.new(5.seconds) 
      ls.improves = false 
      ls.stop_criterion.should be_true 
    end

    it "returns true if has the internal timer has finished" do 
      ls.timer = Timer.new(0.1.seconds)
      ls.improves = true 
      ls.stop_criterion.should be_false 
      sleep(0.1) 
      ls.stop_criterion.should be_true
    end   
  end 

  describe "search_loop" do 
    it "yields to a block for the given duration" do 
      tic = Time.monotonic 
      ls.search_loop do 
      end
      (Time.monotonic - tic).to_f.should be_close ls.duration.to_f,1e-3 
    end 
    
    it "tracks into logger the iterations and logs the best solution found" do 
      logger.ioevo.as(IO::Memory).clear 
      ls.duration = 0.1.seconds

      ls.search_loop do 
        sleep(0.1) 
      end
      
      ls.logger.it_cnt.should eq 1
      
      lines = logger.ioevo.to_s.each_line.to_a
      lines.size.should eq 1
    end 

  end 

  describe "update_best" do 
    it "updates the best fitness and best value object" do 
      thief = Thief.new([0,1,2,3],[false])
      fit   = 0.0 
      
      ls.best.should eq Eil51::ASOL  
      ls.update_best(thief,fit)
      ls.best.should eq thief 
      ls.best.should_not be thief
    end 
  end 

end 
