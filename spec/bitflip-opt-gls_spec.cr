require "./spec-helper" 


class BitFlipOptGLS 
  property localsearch
  property logger
  property penalty 
  property obj_fun
  property lambda
  property ttp
end 


describe "OptBitFlipGLS" do 
  logger = Logger.new 

  optbfgls = BitFlipOptGLS.new(
    Eil51::INST,0.1.seconds,->Eil51::INST.fitness(Thief))

  describe_search(optbfgls)
 
  describe "new" do 
    it "creates a guided local search using the BitFlip-Opt search" do 
      ls = BitFlipOptGLS.new(
        Eil51::INST,
        0.5.seconds, 
        ->Eil51::INST.fitness(Thief),
        logger)

      ls.lambda.should  eq 0.0 
      ls.logger.should  eq logger 
      ls.penalty.should be_a Penalization
      ls.obj_fun.should eq ->Eil51::INST.fitness(Thief)
      ls.duration.should eq 0.5.seconds 
      ls.ttp.should     be Eil51::INST 
      ls.localsearch.should be_a BitFlipOptLS 
    end 
  end 

  describe "search" do 
    it "should update @lambda value" do 
      optbfgls.search(Eil51::ASOL)
      optbfgls.lambda.should_not eq 0.0
    end  
  end 
end 
