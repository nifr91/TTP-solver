require "./spec-helper"

describe "OptLS" do 
  
  optls = OptLS.new(Eil51::INST,0.1.seconds,->Eil51::INST.fitness(Thief))
  
  describe "opt_neighbors_of" do 
    it "iterates over all opt-neighbors" do 

      neighborhood = Eil51::ASOL_2OPT_NEIGHBORHOOD.dup
      cnt = 0
      optls.each_neighbor_of(Eil51::ASOL) do |neighbor| 
        found = neighborhood.delete(neighbor)
        cnt += 1 
      end 
      cnt.should  eq Eil51::ASOL_2OPT_NEIGHBORHOOD.size 
      neighborhood.size.should eq 0


      neighborhood = Eil51::BSOL_2OPT_NEIGHBORHOOD.dup.uniq
      cnt = 0
      optls.each_neighbor_of(Eil51::BSOL) do |neighbor| 
        found = neighborhood.delete(neighbor)
        pp neighbor.tour[0..5] if found.nil?
        cnt += 1 
      end 
      cnt.should  eq Eil51::BSOL_2OPT_NEIGHBORHOOD.size 
      neighborhood.size.should eq 0
    end 
  end 

  describe_search optls

  describe "search" do 
    it "terminates when it cannot improve more" do 
      optls.duration = 1.seconds
      tic = Time.monotonic  
      optls.search(Eil51::ASOL)
      (Time.monotonic - tic).to_f.should_not be_close 1,1e-1
    end 
  end 
end
