require "./spec-helper.cr" 

describe "BitFlipLS" do 
  
  bfls = BitFlipLS.new(Eil51::INST,0.1.seconds,->Eil51::INST.fitness(Thief))
  

  describe "neighborhood" do 
    it "iterates over all the neighborhood" do
          
      neighborhood = Eil51::ASOL_BF_NEIGHBORHOOD.dup

      cnt = 0
      bfls.each_neighbor_of(Eil51::ASOL) do |neighbor|
        founded = neighborhood.delete(neighbor)
        cnt += 1
      end   
      cnt.should               eq Eil51::ASOL_BF_NEIGHBORHOOD.size
      neighborhood.size.should eq 0

      neighborhood = Eil51::BSOL_BF_NEIGHBORHOOD.dup
      cnt = 0
      bfls.each_neighbor_of(Eil51::BSOL) do |neighbor|
        founded = neighborhood.delete(neighbor)
        cnt += 1
      end   
      cnt.should               eq Eil51::BSOL_BF_NEIGHBORHOOD.size
      neighborhood.size.should eq 0
    end 
  end 
  
  describe_search bfls

  describe "search" do 
    it "terminates when it cannot improve more" do 
      bfls.duration = 1.seconds
      tic = Time.monotonic  
      bfls.search(Eil51::ASOL)
      (Time.monotonic - tic).to_f.should_not be_close 1,1e-1
    end 
  end 
end 
