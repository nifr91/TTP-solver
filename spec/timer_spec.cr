require "./spec-helper" 

describe "Timer" do 
  duration = 0.1.seconds 

  describe "new" do 
    it "creates a new timer" do 
      timer = Timer.new(duration) 
      tic   = Time.monotonic.to_f 
      timer.start_time.to_f.should be_close tic,1e-3
      timer.time_limit.to_f.should be_close (tic + duration.to_f),1e-3
    end 
  end

  describe "elapsed_time" do 
    it "returns the  elapsed time from creation" do 
      timer = Timer.new(duration)
      timer.elapsed_time.to_f.should be_close 0.0,1e-3
      sleep(duration)
      timer.elapsed_time.to_f.should be_close duration.to_f,1e-3
    end 
  end  

  describe "progress" do 
    it "returns the percentage of progress of duration completion" do 
      timer = Timer.new(duration)
      timer.progress.should be_close 0.0, 1e-3
      sleep(duration)
      timer.progress.should be_close 1.0, 1e-3
    end 
  end 
  
  describe "finished?" do 
    it "returns if the duration time has elapsed" do 
      timer = Timer.new(duration)
      timer.finished?.should be_false 
      sleep(duration)
      timer.finished?.should be_true 
    end 
  end 

  describe "time_left" do 
    it "returns the remaining time to completion" do 
      timer = Timer.new(duration)
      timer.time_left.to_f.should be_close duration.to_f,1e-3
      sleep(duration)
      timer.time_left.to_f.should be_close 0.0,1e-3
      sleep(duration)
      timer.time_left.to_f.should eq 0.0
    end 
  end 

end 
