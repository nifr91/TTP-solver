require "spec"
require "./spec-helper"


describe "City" do 

  describe "new" do 
    it "creates with values" do 
      city = City.new id: 1, x: 1.0, y:2.0
      
      city.x.should  eq(1.0)
      city.y.should  eq(2.0)
      city.id.should eq(1)
    end  
  end 

  describe "self.euc_dist" do 
    it "calculates the euclidean distance between cities" do 
      acity = City.new(0,0.0,1.0)
      bcity = City.new(1,0.0,0.0)

      City.euc_dist(acity,bcity).should eq(1.0)
      
      ax = rand(1.0)
      ay = rand(1.0)
      acity = City.new(2,ax,ay)
      
      bx = rand(1.0)
      by = rand(1.0)
      bcity = City.new(3,bx,by) 
      
      x = (ax - bx)
      y = (ay - by) 
      dist = Math.sqrt((x*x) + (y*y))


      City.euc_dist(acity,bcity).should be_close(dist,1e12) 
    end

    it "should be commutative" do 
      acity = City.new(2,rand(1.0),rand(1.0))
      bcity = City.new(3,rand(1.0),rand(1.0)) 

      City.euc_dist(acity,bcity).should eq(City.euc_dist(bcity,acity))
      
    end  
  end 

  describe "clone" do 
    it "creates a deep copy of self" do 
      original = City.new(0,0.0,0.0)
      clone = original.clone 
      
      clone.id.should     eq original.id 
      clone.x.should      eq original.x 
      clone.y.should      eq original.y

    end 
  end 
end 
