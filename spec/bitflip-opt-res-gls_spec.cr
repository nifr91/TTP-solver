require "./spec-helper" 


class BitFlipOptResGLS 
  property restart_span 
  property lkh_exe 
  property restart_timer 
  property logger
end 

describe "BitFlipOptResGLS"  do 

  logger = Logger.new 

  bfoptresgls = BitFlipOptResGLS.new(
    Eil51::INST,
    0.1.seconds,
    Eil51::OBJ_FUN,
    0.01.seconds,
    Files::LKH_EXE,
    logger)

  describe "new" do 
    it "creates a new object" do 
      ls  = BitFlipOptResGLS.new(
        Eil51::INST,
        0.1.seconds,
        Eil51::OBJ_FUN,
        0.01.seconds,
        Files::LKH_EXE,
        logger)

      ls.restart_timer.time_left.to_f.should be_close 0.01,1e-3
      ls.restart_span.should eq 0.01.seconds
      ls.lkh_exe.should      eq Files::LKH_EXE
    end 
  end

 describe_search bfoptresgls  

 describe "search" do 
    describe "a good solution must trigger a restart" do
      sol = bfoptresgls.search(Eil51::BSOL)
      bfoptresgls.logger.res_cnt.should be >= 1
    end 
 end 


end 
