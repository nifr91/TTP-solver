
require "./eil51_neighborhoods"

module Eil51


INST = TTP.load(Files::EIL51)

OBJ_FUN = ->INST.fitness(Thief)

NAME      = "eil51-TTP"
KP_TYPE   = "uncorrelated"
NCITIES   = 51
NITEMS    = 50
MX_WGT    = 22264
MX_VEL    = 1.0
MN_VEL    = 0.1
RRATIO    = 22.33
EDGE_TYPE = "CEIL_2D"
NU        = (MX_VEL - MN_VEL) / MX_WGT.to_f

CITIES = [
  City.new(0  , 37.0 , 52.0 ) ,
  City.new(1  , 49.0 , 49.0 ) ,
  City.new(2  , 52.0 , 64.0 ) ,
  City.new(3  , 20.0 , 26.0 ) ,
  City.new(4  , 40.0 , 30.0 ) ,
  City.new(5  , 21.0 , 47.0 ) ,
  City.new(6  , 17.0 , 63.0 ) ,
  City.new(7  , 31.0 , 62.0 ) ,
  City.new(8  , 52.0 , 33.0 ) ,
  City.new(9  , 51.0 , 21.0 ) ,
  City.new(10 , 42.0 , 41.0 ) ,
  City.new(11 , 31.0 , 32.0 ) ,
  City.new(12 , 5.0  , 25.0 ) ,
  City.new(13 , 12.0 , 42.0 ) ,
  City.new(14 , 36.0 , 16.0 ) ,
  City.new(15 , 52.0 , 41.0 ) ,
  City.new(16 , 27.0 , 23.0 ) ,
  City.new(17 , 17.0 , 33.0 ) ,
  City.new(18 , 13.0 , 13.0 ) ,
  City.new(19 , 57.0 , 58.0 ) ,
  City.new(20 , 62.0 , 42.0 ) ,
  City.new(21 , 42.0 , 57.0 ) ,
  City.new(22 , 16.0 , 57.0 ) ,
  City.new(23 , 8.0  , 52.0 ) ,
  City.new(24 , 7.0  , 38.0 ) ,
  City.new(25 , 27.0 , 68.0 ) ,
  City.new(26 , 30.0 , 48.0 ) ,
  City.new(27 , 43.0 , 67.0 ) ,
  City.new(28 , 58.0 , 48.0 ) ,
  City.new(29 , 58.0 , 27.0 ) ,
  City.new(30 , 37.0 , 69.0 ) ,
  City.new(31 , 38.0 , 46.0 ) ,
  City.new(32 , 46.0 , 10.0 ) ,
  City.new(33 , 61.0 , 33.0 ) ,
  City.new(34 , 62.0 , 63.0 ) ,
  City.new(35 , 63.0 , 69.0 ) ,
  City.new(36 , 32.0 , 22.0 ) ,
  City.new(37 , 45.0 , 35.0 ) ,
  City.new(38 , 59.0 , 15.0 ) ,
  City.new(39 , 5.0  , 6.0  ) ,
  City.new(40 , 10.0 , 17.0 ) ,
  City.new(41 , 21.0 , 10.0 ) ,
  City.new(42 , 5.0  , 64.0 ) ,
  City.new(43 , 30.0 , 15.0 ) ,
  City.new(44 , 39.0 , 10.0 ) ,
  City.new(45 , 32.0 , 39.0 ) ,
  City.new(46 , 25.0 , 32.0 ) ,
  City.new(47 , 25.0 , 55.0 ) ,
  City.new(48 , 48.0 , 28.0 ) ,
  City.new(49 , 56.0 , 37.0 ) ,
  City.new(50 , 30.0 , 40.0 )
]

ITEMS = [
  Item.new(0  , 119 , 1    , 1  ) ,
  Item.new(1  , 187 , 896  , 2  ) ,
  Item.new(2  , 997 , 367  , 3  ) ,
  Item.new(3  , 22  , 690  , 4  ) ,
  Item.new(4  , 347 , 613  , 5  ) ,
  Item.new(5  , 522 , 874  , 6  ) ,
  Item.new(6  , 730 , 122  , 7  ) ,
  Item.new(7  , 705 , 486  , 8  ) ,
  Item.new(8  , 232 , 823  , 9  ) ,
  Item.new(9  , 554 , 463  , 10 ) ,
  Item.new(10 , 675 , 589  , 11 ) ,
  Item.new(11 , 970 , 325  , 12 ) ,
  Item.new(12 , 407 , 1000 , 13 ) ,
  Item.new(13 , 276 , 776  , 14 ) ,
  Item.new(14 , 331 , 123  , 15 ) ,
  Item.new(15 , 361 , 356  , 16 ) ,
  Item.new(16 , 798 , 305  , 17 ) ,
  Item.new(17 , 427 , 47   , 18 ) ,
  Item.new(18 , 439 , 645  , 19 ) ,
  Item.new(19 , 91  , 139  , 20 ) ,
  Item.new(20 , 650 , 247  , 21 ) ,
  Item.new(21 , 10  , 211  , 22 ) ,
  Item.new(22 , 838 , 100  , 23 ) ,
  Item.new(23 , 725 , 627  , 24 ) ,
  Item.new(24 , 843 , 760  , 25 ) ,
  Item.new(25 , 137 , 776  , 26 ) ,
  Item.new(26 , 977 , 475  , 27 ) ,
  Item.new(27 , 718 , 937  , 28 ) ,
  Item.new(28 , 99  , 101  , 29 ) ,
  Item.new(29 , 609 , 281  , 30 ) ,
  Item.new(30 , 819 , 751  , 31 ) ,
  Item.new(31 , 97  , 530  , 32 ) ,
  Item.new(32 , 975 , 240  , 33 ) ,
  Item.new(33 , 226 , 248  , 34 ) ,
  Item.new(34 , 241 , 768  , 35 ) ,
  Item.new(35 , 584 , 31   , 36 ) ,
  Item.new(36 , 348 , 613  , 37 ) ,
  Item.new(37 , 475 , 881  , 38 ) ,
  Item.new(38 , 178 , 367  , 39 ) ,
  Item.new(39 , 641 , 712  , 40 ) ,
  Item.new(40 , 898 , 410  , 41 ) ,
  Item.new(41 , 250 , 157  , 42 ) ,
  Item.new(42 , 391 , 824  , 43 ) ,
  Item.new(43 , 101 , 242  , 44 ) ,
  Item.new(44 , 636 , 472  , 45 ) ,
  Item.new(45 , 87  , 722  , 46 ) ,
  Item.new(46 , 652 , 775  , 47 ) ,
  Item.new(47 , 867 , 371  , 48 ) ,
  Item.new(48 , 54  , 337  , 49 ) ,
  Item.new(49 , 509 , 885  , 50 )
]

DIST = Array.new(CITIES.size) do |r|
  Array.new(CITIES.size) do |c|
    City.euc_dist(CITIES[r],CITIES[c]) 
  end 
end 


ASOL = Thief.new(
  tour: [
   0  , 50 , 9  , 36 , 39 , 34 , 28 , 27 , 32 , 31 , 25 , 33 , 10 , 44 , 12 ,
   40 , 1  , 30 , 41 , 45 , 21 , 20 , 17 , 18 , 49 , 24 , 29 , 4  , 11 , 7  ,
   43 , 48 , 23 , 26 , 13 , 8  , 5  , 15 , 22 , 16 , 38 , 19 , 3  , 2  , 14 ,
   42 , 47 , 35 , 37 , 46 , 6
  ],
  plan: [ 
    true  , false , false , false , false   , false , false , false , true  ,
    false , false , false , false , false   , false , true  , false , true  ,
    false , true  , true  , false , true    , false , false , true  , false ,
    false , false , false , false , false   , true  , false , false , true  ,
    false , false , true  , false , false   , false , false , true  , true  ,
    false , false , false , true  , false
  ])

ASOL_FIT = -39637.852748463105
ASOL_CITIES_WGT = [
  0 , 1 , 0   , 0   , 0  , 0   , 0   , 0   , 0  , 823 , 0 , 0   , 0   , 0 ,
  0 , 0 , 356 , 0   , 47 , 0   , 139 , 247 , 0  , 100 , 0 , 0   , 776 , 0 ,
  0 , 0 , 0   , 0   , 0  , 240 , 0   , 0   , 31 , 0   , 0 , 367 , 0   , 0 ,
  0 , 0 , 242 , 472 , 0  , 0   , 0   , 337 , 0
  ]
ASOL_ACC_WGT = [
  0    , 0    , 823  , 854  , 1221 , 1221 , 1221 , 1221 , 1221 , 1221 , 1221 ,
  1461 , 1461 , 1703 , 1703 , 1703 , 1704 , 1704 , 1704 , 2176 , 2423 , 2562 ,
  2562 , 2609 , 2946 , 2946 , 2946 , 2946 , 2946 , 2946 , 2946 , 2946 , 3046 ,
  3822 , 3822 , 3822 , 3822 , 3822 , 3822 , 4178 , 4178 , 4178 , 4178 , 4178 ,
  4178 , 4178 , 4178 , 4178 , 4178 , 4178 , 4178
]
ASOL_TIME = 2016.1599976920336
ASOL_PROFIT = 5383.0
ASOL_DIST   = 1789.0

ASOL_REM_DIST = Array.new(NCITIES,0.0)
ASOL_REM_DIST[0] = ASOL_DIST
(1 ... NCITIES).each do |i| 
  ASOL_REM_DIST[i] = ASOL_REM_DIST[i-1] - DIST[ASOL.tour[i-1]][ASOL.tour[i]]
end 



BSOL = Thief.new(
  tour: [ 
    0  , 21 , 1  , 15 , 33 , 29 , 9  , 38 , 32   , 44 , 14 , 36 , 16 , 43 ,
    41 , 39 , 18 , 40 , 12 , 24 , 13 , 23 , 42   , 22 , 6  , 25 , 7  , 30 ,
    27 , 2  , 35 , 34 , 19 , 28 , 20 , 49 , 8    , 48 , 37 , 4  , 11 , 46 ,
    3  , 17 , 5  , 47 , 26 , 50 , 45 , 10 , 31
  ],
  plan: [
    true  , false , true  , false , true  , true , true  , true  , false ,
    true  , true  , true  , false , false , true , true  , true  , true  ,
    true  , true  , true  , false , true  , true , true  , true  , true  ,
    true  , true  , true  , true  , false , true , true  , false , true  ,
    true  , false , false , true  , true  , true , false , false , true  ,
    false , true  , true  , false , true
  ])

BSOL_FIT = 6897.33782124392
BSOL_PROFIT = 21067.0
BSOL_CITIES_WGT = [
  0   , 1   , 0   , 367 , 0   , 613 , 874 , 122 , 486 , 0   , 463 , 589 ,
  325 , 0   , 0   , 123 , 356 , 305 , 47  , 645 , 139 , 247 , 0   , 100 ,
  627 , 760 , 776 , 475 , 937 , 101 , 281 , 751 , 0   , 240 , 248 , 0   ,
  31  , 613 , 0   , 0   , 712 , 410 , 157 , 0   , 0   , 472 , 0   , 775 ,
  371 , 0   , 885
]
BSOL_ACC_WGT = [
  0     , 247   , 248   , 371   , 611   , 712   , 712   , 712   , 712   ,
  712   , 712   , 743   , 1099  , 1099  , 1509  , 1509  , 1556  , 2268  ,
  2593  , 3220  , 3220  , 3320  , 3477  , 3477  , 4351  , 5111  , 5233  ,
  5514  , 5989  , 5989  , 5989  , 6237  , 6882  , 7819  , 7958  , 7958  ,
  8444  , 8815  , 9428  , 9428  , 10017 , 10017 , 10384 , 10689 , 11302 ,
  12077 , 12853 , 13738 , 14210 , 14673 , 15424
]
BSOL_TIME = 634.5571956451447
BSOL_DIST = 471.0

BSOL_REM_DIST = Array.new(NCITIES,0.0)
BSOL_REM_DIST[0] = BSOL_DIST
(1 ... NCITIES).each do |i| 
  BSOL_REM_DIST[i] = BSOL_REM_DIST[i-1] - DIST[BSOL.tour[i-1]][BSOL.tour[i]]
end 

# Neighborhoods ---------------------------------------------------------------

end 
