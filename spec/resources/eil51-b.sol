# 2018-04-04 04:21:36 -05:00
# Solución de Travelling Salesman Problem
# Instancia         : eil51_n50_uncorr_10.ttp
# Recorrido Inicial : lkh
# Plan Inicial      : greedy
# Método            : gs_opt_bf
# Duración          : 00:05:00.000204237

# Tour
1,22,2,16,34,30,10,39,33,45,15,37,17,44,42,40,19,41,13,25,14,24,43,23,7,26,8,31,28,3,36,35,20,29,21,50,9,49,38,5,12,47,4,18,6,48,27,51,46,11,32
# Items
1,0,1,0,1,1,1,1,0,1,1,1,0,0,1,1,1,1,1,1,1,0,1,1,1,1,1,1,1,1,1,0,1,1,0,1,1,0,0,1,1,1,0,0,1,0,1,1,0,1
# Fitness
6897.33782124392
