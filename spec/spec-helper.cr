require "spec"
require "./resources/files" 
require "./resources/eil51"
require "../src/*"


macro describe_search(ls)
  describe "search" do 

  it "returns a different object" do 
    sol = {{ls}}.search(Eil51::ASOL)
    sol.should_not be Eil51::ASOL
  end 

  it "should not modify the initial solution" do 
    aux = Eil51::ASOL.clone 
    sol = {{ls}}.search(aux)

    aux.should eq Eil51::ASOL
  end 

  it "returns an equal or better solution" do 
    sol = {{ls}}.search(Eil51::ASOL)
    Eil51::INST.fitness(sol).should be >= Eil51::ASOL_FIT 

    sol = {{ls}}.search(Eil51::BSOL)
    Eil51::INST.fitness(sol).should be >= Eil51::BSOL_FIT 
  end 

  it "should improve a bad solution" do 
    sol = {{ls}}.search(Eil51::ASOL)
    Eil51::INST.fitness(sol).should be > Eil51::ASOL_FIT 
  end 

  it "returns a valid solution" do 
    sol = {{ls}}.search(Eil51::ASOL) 
    Eil51::INST.validate_thief(sol.to_s).should eq TTP::ThiefError::None
  end   

  end 
end 
