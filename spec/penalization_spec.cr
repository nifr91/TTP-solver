require "./spec-helper"

class DummyPenalization < Penalization 

  def initialize(@ttp : TTP) 
    @ary = [0u64] 
  end 
  
  def call(s : Thief)
    @ary[0] 
  end 

  def update(s : Thief)
    @ary[0] += 1
  end

  def reset
    @ary[0] = 0u64
  end 
end

macro describe_penalization(penalizationclass)  

  describe "{{penalizationclass}}" do 
    penalty = {{penalizationclass}}.new(Eil51::INST)

    describe "new" do 
      it "initialize a new penalization" do 
        p = {{penalizationclass}}.new(Eil51::INST) 
        p.should be_a Penalization
      end 
    end 

    describe "call" do 
      it "returns the penalty of the given solution" do 
        penalty.call(Eil51::ASOL).should eq 0 
      end 
    end 

    describe "update" do 
      it "updates the penalty" do 
        penalty.update(Eil51::ASOL)
        penalty.call(Eil51::ASOL).should eq 1 
        
        r = rand(10)
        r.times{ penalty.update(Eil51::ASOL) } 

        penalty.call(Eil51::ASOL).should eq (1+r)  

      end 
    end

    describe "reset" do 
      it "resets the penalty of features values" do 
        penalty.reset 
        penalty.call(Eil51::ASOL).should eq 0 
        penalty.update(Eil51::ASOL)
        penalty.call(Eil51::ASOL).should eq 1 
        penalty.reset 
        penalty.call(Eil51::ASOL).should eq 0 
      end 
    end  
  end 

end

describe_penalization DummyPenalization 
describe_penalization TourPenalization
