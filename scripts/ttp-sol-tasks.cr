#!/usr/bin/env crystal

# Ricardo Nieto Fuentes 
# nifr91@gmail.com 

# Script para realizar la comparación de diferentes métodos  al resolver el
# problema del larón viajero TTP. Se asumen que el en la raíz del directorio se
# encuentra la siguiente estructura: 
#
# /
# |- ttp-cli.exe
# |- lkh.exe
# |- instances/ 
# |- out/
#
#
# Escribe los resultados a STDOUT
#
# Los parámetros del programa son: 

repetitions   = 30          # Número de repeticiones
duration      = 43200       # Duración de la búsqueda
evo_span      = 1440        # Tiempo entre mediciones de la evolución
res_span      = 300         # Tiempo sin mejora para forzar un reinicio
track_evo     = true        # Si se desea segir la mejora
exp_id        = "art-01"    # Etiqueta del experimento
gens          = 500         # Número de generaciones
min_ls_time   = 0.5         # duración mínima de la búsqueda local
min_dist      = 0.05        # Porcentaje mínimo en de la distancia
output_dir    = "out"       # Directorio que contendrá los archivos de resultados
instances_dir = "instances" # Directorio que contiene las instancias
lkh_exe       = "lkh.exe"   # Ejecutable a utlizar

basename = [
  # "berlin52_n510_uncorr_10",
  "eil51_n500_uncorr_10",
  # "eil101_n1000_uncorr_10",
  "lin105_n1040_uncorr_10",
  "ch150_n1490_uncorr_10",
  # "pr152_n1510_uncorr_10",
  "kroA200_n1990_uncorr_10",
  # "d198_n1970_uncorr_10",
  "pr264_n2630_uncorr_10",
  # "gil262_n2610_uncorr_10",
  # "a280_n2790_uncorr_10",
  "lin318_n3170_uncorr_10",
  "rd400_n3990_uncorr_10",
  # "berlin52_n510_bounded-strongly-corr_10",
  "eil51_n500_bounded-strongly-corr_10",
  # "eil101_n1000_bounded-strongly-corr_10",
  "lin105_n1040_bounded-strongly-corr_10",
  "ch150_n1490_bounded-strongly-corr_10",
  # "pr152_n1510_bounded-strongly-corr_10",
  "kroA200_n1990_bounded-strongly-corr_10",
  # "d198_n1970_bounded-strongly-corr_10",
  "pr264_n2630_bounded-strongly-corr_10",
  # "gil262_n2610_bounded-strongly-corr_10",
  # "a280_n2790_bounded-strongly-corr_10",
  "lin318_n3170_bounded-strongly-corr_10",
  "rd400_n3990_bounded-strongly-corr_10"
]

instances = basename.map do |name| 
  "#{instances_dir}/#{name}.ttp" 
end 

# Métodos a utilizar para resolver las instancias
methods     = [
  ["resgls"    , "lkh" , "packiterative"] ,
  ["dma"       , "lkh" , "packiterative"] ,
  ["elitistma" , "lkh" , "packiterative"] ,
] 



# Programa principal ==========================================================

puts <<-TEXT
#
# Experimento #{exp_id}
# 
# Se comparan los resultados obtenidos por la búsqueda local por escalada 
# al emplear diferentes inicializaciones. 
#
# Las instancias a resolver son : 
#
TEXT

basename.each{|name| puts ("#  * #{name}") }

puts <<-TEXT
#
# Se utilizan los siguientes métodos : 
#
TEXT

methods.each{|(met,ti,pi)| puts("#  - #{met}-#{ti}-#{pi}")}

puts <<-TEXT
#
# Se realizan #{repetitions} repeticiones de cada ejecución
# cada una tiene una duración de #{duration} segundos 
# se considera un tiempo de reinicio de #{res_span} 
# #{(track_evo ? "se mide la evolución cada #{evo_span} segundos" : "")}
# se consideran #{gens} generaciones
# el tiempo mínimo dedicado a cada búsqueda local es #{min_ls_time} segundos
#
# La estructura del directorio esperada es: 
#
# /
# |- ttp-cli.exe
# |- #{lkh_exe} 
# |- #{instances_dir}
# |- #{output_dir}
#
TEXT


(0 ... instances.size).each do |index|
  inst = instances[index] 
  name = basename[index] 

  puts 
  puts "#-#{name}" 
  puts 

  repetitions.times do |rep| 
    methods.each do |(met,ti,pi)| 
      fname        = "out/#{name}_-_#{met}-#{ti}-#{pi}_-_#{exp_id}"
      output       = "#{fname}.sol_#{"%03d" % rep}"
      evo_filename = "#{fname}.evo_#{"%03d" % rep}"

      str = String.build do |io| 
        io << "./ttp-cli.exe"                << " "
        io << "--if #{inst}"                 << " "
        io << "--of #{output}"               << " "
        io << "--evo-span #{evo_span}"       << " " if track_evo
        io << "--evo-file #{evo_filename}"   << " " if track_evo
        io << "--tour-init #{ti}"            << " "
        io << "--plan-init #{pi}"            << " "
        io << "--duration  #{duration}"      << " "
        io << "--lkh-file lkh.exe"           << " "
        io << "--res-span #{res_span}"       << " "
        io << "--method #{met}"              << " "
        io << "--gens #{gens}"               << " "
        io << "--min-ls-time #{min_ls_time}" << " "
        io << "--min-dist #{min_dist}"
      end 

      puts str
    end  
  end 

end 
